=== HM-Addon - Best Contact Addon for VOC Web Application ===
Contributors: 
Donate link: https://tareq.co/donate/
Tags: HM-Addon, MERGE ENTERPRISE COMPANY LIMITED, VOC, HostMerge, BeerSting, beer.sting, VOC
Requires at least: 5.6
Requires PHP: 5.6
Tested up to: 1.0
Stable tag: 1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The easiest & fastest Contact Form on WordPress. Multiple templates, drag-&-drop live builder, submission listing, reCaptcha & more!

== Description ==

= Plugin Features =

= Demo Site =

== Installation ==

== Frequently Asked Questions ==

== Screenshots ==


== Changelog ==

= Version 1.0 (6 Jan, 2019) =
 * First beta version

== Upgrade Notice ==

Nothing here right now
