/**
 * 
 	  @author BeerSting <br>
 	     * <b>The MIT License (MIT) Copyright: </b><br>
		 * Copyright (c) 2022, BeerSting<br>
		 * 
		 * <b>Create by: </b><br>
		 * Yoottapong Wongwiwut<br>  
		 * 
		 * <b>Create Date: </b><br>
		 *  Jan 07 2022<br>
		 * 
		 * <b>Email: </b><br>
		 * <A href="mailto:beer.sting@gmail.com">beer.sting@gmail.com</A><br> 
	  @version 1.0
 * 
 */

function getLANGUAGE(lang) {
    lang = (lang) ? lang : "en";
    return lang;
}

function toString(val, returnText) {
    return (val) ? val : returnText;
}

//********************** IO
function getType(fullname) {
    return this.getExtension(fullname);
}

function getExtension(fullname) {
    var ext = fullname.substring(fullname.lastIndexOf(".") + 1, fullname.length);
    ext = ext.toLowerCase();
    return ext;
}

function getName(fullname) {
    var name = fullname;
    if (name != null) {
        var index = fullname.lastIndexOf('\\');
        if (index != -1) {
            name = fullname.substring(index + 1);
        }
        index = fullname.lastIndexOf('/');
        if (index != -1) {
            name = fullname.substring(index + 1);
        }
    }
    return name;
}


//********************** Custom Formater
function DATE_KEY(cellvalue, options, rowObject) {
    var val = cellvalue;
    if (cellvalue) {
        if (cellvalue.indexOf("-") > -1) {
            val = cellvalue.substr(0, 4) + '-' + cellvalue.substr(5, 2) + '-' + cellvalue.substr(8, 2);
        } else if (cellvalue.indexOf("/") == -1) {
            val = cellvalue.substr(8, 2) + '/' + cellvalue.substr(5, 2) + '/' + cellvalue.substr(0, 4);
        }
    }
    return (val == null) ? "" : val;
}

function getParseFloat(val) {
    try {
        if (val) {
            val = parseFloat(val);
        } else {
            val = 0;
        }
    } catch (e) {
        val = 0;
    }
    return val;
}


//**************************** Custom Function         
function getVal(jqTxtSelecter) {
    if (jQuery(jqTxtSelecter).length > 0) {
        return jQuery(jqTxtSelecter).val();
    }
}


function generateHTMLMapper(data) {
    generateHTMLMapper("", data);
}

function generateHTMLMapper(HtmlFormID, data) {
    if (data.length) {
        for (i in data) {
            var object = data[i];
            for (key in object) {
                keyControlSelecter = key;
                if (key.indexOf(".") > 0) {
                    keyControlSelecter = "'" + key + "'";
                }
                setHTMLControl(object[key], jQuery(((HtmlFormID) ? 'FORM#' + HtmlFormID : '') + ' [name=' + keyControlSelecter + ']'));
            }
        }
    } else {
        var object = data;
        for (key in object) {
            keyControlSelecter = key;
            if (key.indexOf(".") > 0) {
                keyControlSelecter = "'" + key + "'";
            }
            setHTMLControl(object[key], jQuery(((HtmlFormID) ? 'FORM#' + HtmlFormID : '') + ' [name=' + keyControlSelecter + ']'));
        }
    }
}

function setHTMLControl(val, jqSelecter) {
    if (jqSelecter.length > 0) {
        var control = jqSelecter;
        if (control.attr('type') == 'text') {
            try {
                if (typeof val == 'string') {
                    if (control[0].tagName == 'LABEL') {
                        if (val) {
                            control.text(val);
                        } else {
                            control.text(String.fromCharCode(160));
                        }
                    } else {
                        control.val(val);
                    }
                } else {
                    control.val(val);
                }
            } catch (err) {
                alert(err.message);
            }
        } else if (control[0].tagName == 'LABEL') {
            if (val) {
                control.text(val);
            } else {
                control.text(String.fromCharCode(160));
            }
        }
        else if (control.attr('type') == 'radio') {
            try {
                control.filter("[value=" + val + "]").prop("checked", true);
            } catch (err) { alert(err.message); }
        }
        else if (control[0].tagName == 'IMG') {
            try {
                control.prop("src", val);
            } catch (err) { alert(err.message); }
        }
        else if (control.attr('type') == 'checkbox') {
            try {
                if (val) {
                    if (val == "true" || val == "Y") {
                        val = 1;
                    }
                    if (val == "false" || val == "N") {
                        val = 0;
                    }
                }
                if(control.filter("[value=" + val + "]").length > 0){
                	control.prop("checked", false);
                	control.filter("[value=" + val + "]").prop("checked", true);
                }else if (Boolean(val)) {
                    control[0].checked = true;
                }else {
                    control[0].checked = false;
                }
            } catch (err) { alert(err.message); }
        }
        else if (control.attr('type') == 'select' || (control[0].type && control[0].type.indexOf("select") != -1)) {
            try {
                if (typeof val == 'string' || typeof val == 'number') {
                    if (jQuery._data(control[0], 'events')) {
                        if (jQuery._data(control[0], 'events')["change"]) {
                            jQuery(control.selector + " option[value='" + val + "']").removeAttr('selected');
                            jQuery(control.selector + " option[value='" + val + "']").prop("selected", "selected");
                            control.trigger("change");
                        }
                    } else {
                        if (jQuery(control.selector + " option[value='" + val + "']").length > 0){
                            if (jQuery(control.selector + " option[value='" + val + "']")[0].selected == false) {
                                jQuery(control.selector + " option").removeAttr('selected');
                                jQuery(control.selector + " option[value='" + val + "']").prop("selected", "selected");
                            }
                        }
                    }
                } else if (val) {
                    control.empty();
                    jQuery.each(val, function (i) {
                       control.append(jQuery("<option " + ((i == 0) ? "selected=\"selected\"" : "") + "></option>").attr("value", val[i]['value']).text(val[i]['text']));
                    });
                }

            } catch (err) { alert(err.message); }
        }
        else if (control.attr('type') == 'file') {
            //... not set value
        }
        else {
            control.val(val);
        }
    }
}

//Get Control Comboboxe Cascade
function getDatabaseCascade(control, table_join, condition_join, action, idFormManage, thisControl) {
    var list_control = control.split(",");
    var list_val = [];
    var selecter = "";
    var id = "";
    for (var i = 0; i < list_control.length; i++) {
        var name = list_control[i];
        name = jQuery.trim(name);
        list_control[i] = name;

        var jqTxtSelecter = "";
        if (action == "form") {
            var formId = jQuery(thisControl).parents("FORM")[0].id;
            jqTxtSelecter = "FORM#" + formId + " [name=" + name + "]";
        } else if (action == "grid") {
            eval("id =lastsel_" + table);
            eval("jqTxtSelecter=\"#\"+id+\"_\"+name");
        }
        list_val[i] = getVal(jqTxtSelecter);
        if (i == list_control.length - 1) {
            selecter = jqTxtSelecter;
        }
    }

    jQuery(selecter).attr("disabled", true);
    jQuery.ajax({
        async: false,
        type: "POST",
        url: 'FormController/cascade',
        data: {
            idFormManage: idFormManage, id: id, idForm: jqTxtSelecter,
            list_control: list_control.join(","), list_val: list_val.join(","), action: action,
            table_join: table_join, condition_join: condition_join
        },
        dataType: "json",
        success: function (ajaxResult) {
            if (ajaxResult.value == "Success") {
                jQuery(selecter).attr("disabled", false);
                eval(ajaxResult.text);
            }
            if (ajaxResult.value == "Error") {
                jQuery.jgrid.info_dialog(jQuery.jgrid.errors.errcap, ajaxResult.text, jQuery.jgrid.edit.bClose);
            }
        },
        error: function (res, status, exeption) {
        }
    });
}

function ajax_execute(method_execute, data, success, error) {
    ajax_execute(method_execute, data, success, error, false);
}

function ajax_execute(method_execute, data, success, error, async) {
    var url = method_execute;
    jQuery.ajax({
        async: (async) ? async : false,
        type: "POST",
        url: url,
        data: data,
        success: function (val) {
            if(typeof val == "string"){
                val = jQuery.parseJSON(val);
            }
            if (val.status == "Success") {
                success(val);
            } else {
                if (val.status == "Info" || val.status == "Error") {
                    error(val);
                }
            }
        },
        error: function (res, status, exeption) {
            //  jQuery.jgrid.info_dialog(jQuery.jgrid.errors.errcap,msgDtg[4],jQuery.jgrid.edit.bClose);
        }
    });
}

function ajax_executeMultipart(method_execute, data, success, error, async) {
    var url = method_execute;
    jQuery.ajax({
        async: (async) ? async : false,
        url: url,
        type: 'POST',
        data: data,
        cache: false,
        //dataType: 'json',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function (val) {
            if(typeof val == "string"){
                val = jQuery.parseJSON(val);
            }
            if (val.status == "Success") {
                success(val);
            } else {
                if (val.status == "Info" || val.status == "Error") {
                    error(val);
                }
            }
        },
        error: function (res, status, exeption) {
            //  jQuery.jgrid.info_dialog(jQuery.jgrid.errors.errcap,msgDtg[4],jQuery.jgrid.edit.bClose);
        }
    });
}


function confirmDialog(title, text, yes_text, no_text, callbackYesFunction, callbackNoFunction, parameter) {
    jconfirm.defaults = {
        title: ((title) ? title : ""),
        content: ((text) ? text : ""),
        defaultButtons: {
            ok: {
                text: yes_text,
                action: function () {
                    callbackYesFunction(parameter);
                }
            },
            close: {
                text: no_text,
                action: function () {
                    if (callbackNoFunction) {
                        callbackNoFunction(parameter);
                    }
                }
            },
        },
    }
    
    return $.confirm({
        icon: 'fa fa-question',
        theme: 'bootstrap',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
    });
}

function showDialog(title, text, close_text, type, callbackFunction) {
    return $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        title: ((title) ? title : ""),
        content: ((text) ? text : ""),
        type: ((type) ? type : 'red'),
        typeAnimated: true,
        buttons: {
            close:{
                text: close_text,
                action: function () {
                    if (callbackFunction) {
                        callbackFunction();
                    }
                }
            }
        }
    });
}

if (typeof jQuery !== 'undefined') {
    jQuery.fn.serializeObject = function () {
        var arrayData, objectData;
        arrayData = this.serializeArray();
        objectData = {};
        jQuery.each(arrayData, function () {
            var value;
            if (this.value != null) {
                value = this.value;
            } else {
                value = '';
            }
            if (objectData[this.name] != null) {
                if (!objectData[this.name].push) {
                    objectData[this.name] = [objectData[this.name]];
                }

                objectData[this.name].push(value);
            } else {
                objectData[this.name] = value;
            }
        });

        return objectData;
    }
};
