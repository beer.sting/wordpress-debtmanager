/**
 * 
        @author BeerSting <br>
           * <b>The MIT License (MIT) Copyright: </b><br>
         * Copyright (c) 2017, BeerSting<br>
         * 
         * <b>Create by: </b><br>
         * Yoottapong Wongwiwut<br>  
         * 
         * <b>Create Date: </b><br>
         *  July 07 2017<br>
         * 
         * <b>Email: </b><br>
         * <A href="mailto:beer.sting@gmail.com">beer.sting@gmail.com</A><br> 
      @version 1.0
 * 
 */

var page;
//var pkMapName; // For sent Parameter Mapping PK
var rowData;
var control;
var jqueryControlList = {};
var idFormManage;
var jsGridId;
var table;
var pkTable;
var func;
var action;
var actionBeforeSave;
var actionBeforeDelete;
var actionAfterSave;
var actionBeforeAdd;
var actionBeforeCancel;
var controller;

//*** Config form execute */
function config(mTable, mPKTable, mJsGridId) {
    table = mTable;
    pkTable = mPKTable;
    jsGridId = mJsGridId;
}

function createEvent(mIdFormManage, mController, idBtnAdd, idBtnSave, idBtnCancel) {
    var mTable = table;
    if(!jqueryControlList[table])jqueryControlList[table] = {};
    jqueryControlList[table].table = mTable;
    jqueryControlList[table].idFormManage = mIdFormManage;
    jqueryControlList[table].pkTable = pkTable;
    jqueryControlList[table].jsGridId = jsGridId;
    controller = "../wp-content/plugins/debt-collection/controller/" + mController;

    if (mIdFormManage) {
        idFormManage = mIdFormManage;
        $('#' + idFormManage).validate({
            errorContainer: $('#' + idFormManage + ' #errorContainer'),
            errorLabelContainer: $('#' + idFormManage + ' #errorContainer ul'),
            wrapper: 'li'
        });
    }

    if (idBtnAdd) {
        jQuery("#" + idBtnAdd).click(function () {
            if (event) {
                event.stopPropagation();
                event.preventDefault();
            }
            var isNextStep = true;
            if (actionBeforeAdd) {
                isNextStep = actionBeforeAdd(this);
            }
            if (isNextStep == false) return;

            document.location.href = "Manage?action=Insert";
            control = this;
        });
    }

    if (idBtnSave) {
        jQuery("#" + idBtnSave).click(function () {
            if (event) {
                event.stopPropagation();
                event.preventDefault();
            }
            control = this;
            table = mTable;
            Execute();
        });
    }
    if (idBtnCancel) {
        jQuery("#" + idBtnCancel).click(function () {
            if (event) {
                event.stopPropagation();
                event.preventDefault();
            }
            var isNextStep = true;
            if (actionBeforeCancel) {
                isNextStep = actionBeforeCancel(this);
            }
            if (isNextStep == false) return;
            control = this;
            //document.location.href = "Index";
            jQuery("#" + mIdFormManage).trigger("reset");
            if (jqueryControlList[mIdFormManage]["btnSave"]) {
                $("#" + mIdFormManage + " [name=action]").val("Insert");
                jqueryControlList[mIdFormManage]["btnSave"].text("Save");
            }
        });
    }
    jqueryControlList[table].formManage = (mIdFormManage) ? $('#' + idFormManage) : null;
    jqueryControlList[table].btnAdd = (idBtnAdd) ? jQuery("#" + idBtnAdd) : null;
    jqueryControlList[table].btnSave = (idBtnSave) ? jQuery("#" + idBtnSave) : null;
    jqueryControlList[table].btnCancel = (idBtnCancel) ? jQuery("#" + idBtnCancel) : null;
}

//*** jQuery Confrim*/
var jconfirmTile = "Confrim delete!";
var jconfirmContent = "Confrim delete this data?";
function configJconfirm(mJconfirmTile, mJconfirmContent) {
    jconfirmTile = mJconfirmTile;
    jconfirmContent = mJconfirmContent;
}

//*** jsGrid */
function onItemEditing(args) {
    idFormManage = args.grid._container.context.id;
    table = idFormManage.split("IdJsGrid")[1];
    rowData = args;
    table = jqueryControlList[table].table;
    idFormManage = jqueryControlList[table].idFormManage;
    pkTable = jqueryControlList[table].pkTable;
    jsGridId = jqueryControlList[table].jsGridId;
    // var Parameter = "";
    // var pkName = pkTable.split(',');
    // //var paramMapName = pkMapName.split(',');
    // var object = {};
    // for (var i = 0; i < pkName.length; i++) {
    //     //eval("Parameter +='" + paramMapName[i] + "'+" + "'='" + "+rowData.item." + pkName[i].trim() + "+'&'")
    //     object[pkName[i].trim()] = rowData.item[pkName[i].trim()];
    // }
    generateHTMLMapper(idFormManage, rowData.item);
    $("#" + idFormManage + " [name=action]").val("Update");
    if (jqueryControlList[table]["btnSave"]) {
        jqueryControlList[table]["btnSave"].text("Update");
    }
    //document.location.href = "Manage?action=Update&" + Parameter;
}

function onItemDeleting(args) {
    idFormManage = args.grid._container.context.id;
    table = idFormManage.split("IdJsGrid")[1];
    rowData = args;
    table = jqueryControlList[table].table;
    idFormManage = jqueryControlList[table].idFormManage;
    pkTable = jqueryControlList[table].pkTable;
    jsGridId = jqueryControlList[table].jsGridId;
    confirmDialog(jconfirmTile, jconfirmContent, "Confrim", "Close", Execute, null, "Delete");
}

//*** Ajax action */
var isExecuteMultipart = true;
function setExecuteMultipart(mIsExecuteMultipart) {
    isExecuteMultipart = mIsExecuteMultipart;
}

function Execute(mAction) {
    table = jqueryControlList[table].table;
    idFormManage = jqueryControlList[table].idFormManage;
    pkTable = jqueryControlList[table].pkTable;
    jsGridId = jqueryControlList[table].jsGridId;
    if(table.split("#") > -1){
        table = table.split("#")[0];
    }
    if (isExecuteMultipart) {
        $("#" + idFormManage + " .validation-summary-errors").show();
        $("#" + idFormManage).validate();
        if ($("#" + idFormManage).valid()) {
            $("#" + idFormManage + " .validation-summary-errors").hide();

            data = new FormData(document.getElementById(idFormManage));
            if (page) data.append('page', page);
            data.append('table', table);
            //data.append('ACCESS_KEY', ACCESS_KEY);
            //data.append('LANGUAGE', LANGUAGE);
            //if ($("[name=__RequestVerificationToken]").val()) {
            //    data.append('__RequestVerificationToken', $("[name=__RequestVerificationToken]").val());
            //}

            if (mAction == "Delete") {
                if (actionBeforeDelete) {
                    isNextStep = actionBeforeDelete(this);
                }
            } else {
                if (actionBeforeSave) {
                    isNextStep = actionBeforeSave(this, data);
                }
            }
            if (isNextStep == false) return;


            if (func) {
                data.append('func', func);
            } else {
                data.append('func', "Database");
            }

            if (mAction) {
                data.append('action', mAction);
            } else {
                data.append('action', $("#" + idFormManage + " [name=action]").val());
            }

            if ($("#" + idFormManage + " [name=action]").val() == "Update") {
                var pkName = pkTable.split(',');
                for (var i = 0; i < pkName.length; i++) {
                    if (data.set) {
                        data.set(pkName[i].trim(), $("#" + idFormManage + " [name=" + pkName[i].trim() + "]").val());
                    } else {
                        data.append(pkName[i].trim(), $("#" + idFormManage + " [name=" + pkName[i].trim() + "]").val());
                    }
                }
            }else if(mAction == "Delete" ){
                var pkName = pkTable.split(',');
                for (var i = 0; i < pkName.length; i++) {
                    data.append(pkName[i].trim(), rowData.item[pkName[i]].trim());
                }
            }
            if (pkTable) {
                data.append('PK', pkTable);
            }
            jQuery(control).attr("disabled", true);
            $("#overlay").show();
            ajax_executeMultipart(controller, data, success, error, true);
        }
    } else {
        var frmManage = $("#" + idFormManage);
        frmManage.find(".validation-summary-errors").show();
        frmManage.validate();
        if (frmManage.valid()) {
            frmManage.find(".validation-summary-errors").hide();
            var data = frmManage.serializeObject();
            if (page) data.page = page,
                data.table = table,
                data.action = ((mAction) ? mAction : action);
            //data.ACCESS_KEY = ACCESS_KEY;
            //data.LANGUAGE = LANGUAGE;
            //data.__RequestVerificationToken = $("[name=__RequestVerificationToken]").val();

            var isNextStep = true;
            if (mAction == "Delete") {
                if (actionBeforeDelete) {
                    isNextStep = actionBeforeDelete(this);
                }
            } else {
                if (actionBeforeSave) {
                    isNextStep = actionBeforeSave(this, data);
                }
            }
            if (isNextStep == false) return;

            if (func) {
                data.func = func;
            }

            if (mAction) {
                data.action = mAction;
            } else {
                data.action = $("#" + idFormManage + " [name=action]").val()
            }

            if ($("#" + idFormManage + " [name=action]").val() == "Update") {
                var pkName = pkTable.split(',');
                for (var i = 0; i < pkName.length; i++) {
                    data[pkName[i]] = $("#" + idFormManage + " [name=" + pkName[i].trim() + "]").val();
                }
            }
            data.PK = pkTable;
            jQuery(control).attr("disabled", true);
            $("#overlay").show();
            ajax_execute(controller, data, success, error, true);
        }
    }
    return true;
}

function success(ajaxResult) {
    var isNextStep = true;
    if (actionAfterSave) {
        isNextStep = actionAfterSave(ajaxResult);

    }
    if (isNextStep == false) return;

    if (ajaxResult.comment == "Delete") {
        if ($("#" + jsGridId)) $("#" + jsGridId).jsGrid("loadData");
    } else {
        jQuery(control).attr("disabled", false);
        if ($("#" + idFormManage)) {
            $("#" + idFormManage)[0].reset();
            //$("#" + idFormManage).validate().resetForm();
        }
        if (ajaxResult.comment == "Detail") {
            if ($("#" + jsGridId)) $("#" + jsGridId).jsGrid("loadData");
        }
        //if (ajaxResult.comment == "Update") {
        //setTimeout("document.location.href = 'Index'", 1 * 500);
        $("#" + idFormManage + " [name=action]").val("Insert");
        if (jqueryControlList[table]["btnSave"]) {
            jqueryControlList[table]["btnSave"].text("Save");
        }
        if ($("#" + jsGridId)) $("#" + jsGridId).jsGrid("loadData");
        //}
    }
    $("#overlay").hide();
    toastr["success"](ajaxResult.text);
    //setTimeout("document.location.href = 'Index'", 1 * 500);
}

function error(ajaxResult) {
    $("#overlay").hide();
    toastr["error"](ajaxResult.text);
    //showDialog(PleaseCheckError + ".", ajaxResult.text, Close, "red");
    if (ajaxResult.comment != "Delete") {
        jQuery(control).attr("disabled", false);
    }
}