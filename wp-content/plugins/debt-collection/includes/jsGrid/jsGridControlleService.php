<?php
$document_root = str_replace("\\", "/", explode("wp-content", dirname(__FILE__))[0]);
require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGrid.php');
require_once($document_root . 'wp-content/plugins/debt-collection/includes/database/dbService.php');
require_once($document_root . 'wp-content/plugins/debt-collection/includes/common-function.php');
require_once($document_root . 'wp-content/plugins/debt-collection/includes/database/dialect/mySqlDialect.php');
// require_once($document_root . 'wp-content/plugins/debt-collection/includes/database/dialect/oracleDialect.php');
// require_once($document_root . 'wp-content/plugins/debt-collection/includes/database/dialect/sqlServerDialect.php');

/**
 * 
 	  @author BeerSting <br>
 * <b>The MIT License (MIT) Copyright: </b><br>
 * Copyright (c) 2022, BeerSting<br>
 * 
 * <b>Create by: </b><br>
 * Yoottapong Wongwiwut<br>  
 * 
 * <b>Create Date: </b><br>
 *  Jan 07 2022<br>
 * 
 * <b>Email: </b><br>
 * <A href="mailto:beer.sting@gmail.com">beer.sting@gmail.com</A><br> 
	  @version 1.0
 * 
 */

$jsGrid = new jsGrid();

class jsData
{
    public $itemsCount;
    public $data;
};

function Get()
{
    $SqlQuery = getSqlQuery(true);
    $jdGridData = getDataListValue($SqlQuery["Sql"], $SqlQuery["SqlCount"]);
    //var_dump($SqlQuery);
    //var_dump($jdGridData);
    echo json_encode($jdGridData);
}


function getDataListValue($Sql, $SqlCount)
{
    global $jsGrid;
    $dbservice = new DBservice();
    $listData = null;
    $dataCount = 0;

    $listData = $dbservice->getListValue($Sql, $jsGrid->FormatData);
    $listCount = $dbservice->getListValue($SqlCount);
    if ($listCount != null && count($listCount) > 0) {
        if ($listCount[0]->SqlCount != null) {
            $dataCount = $listCount[0]->SqlCount;
        } else if ($listCount[0]["SQLCOUNT"] != null) {
            $dataCount = $listCount[0]->SQLCOUNT;
        }
    }

    $jsGridData = new jsData();
    $jsGridData->itemsCount = intval($dataCount);
    $jsGridData->data = $listData;
    return $jsGridData;
}

function getSqlQuery($setPaging)
{
    global $jsGrid;
    $Sql = "";
    $SqlCount = "";
    try {
        $page = ($_POST["pageIndex"] != null) ? $_POST["pageIndex"] : 1;
        $rows = ($_POST["pageSize"] != null) ? $_POST["pageSize"] : 99999999;
        $offset = 0;
        $limit = 0;

        $jsGridControl = json_decode(html_entity_decode(stripslashes($_POST["jsGridControl"])));
        if ($_POST["jsGrid"]) {
            $jsGrid = json_decode(html_entity_decode(stripslashes($_POST["jsGrid"])));
            $jsGrid = $jsGrid->jsGrid;
        }

        $formDataObject = ($_POST["formData"] != null) ? json_decode(html_entity_decode(stripslashes($_POST["formData"]))) : null;
        $SqlBuilder = new StringBuilder();
        //*** Initail
        $Sql = "SELECT * FROM  " . ((!$jsGrid->SCHEMA) ? "" : $jsGrid->SCHEMA . ".") . $jsGrid->TABLENAME;
        $SqlCount = "SELECT COUNT(*) AS SqlCount FROM";

        if ($jsGrid->Sql != null) {
            $Sql = $jsGrid->Sql;
        }

        //*** GroupBy
        $SqlGroupby = "";
        $groupByIndex = strrpos(strtolower($Sql), "group by");
        //if ( $groupByIndex  != -1)
        if ($groupByIndex !== false) {
            $SqlGroupby = substr($Sql, $groupByIndex);
            $Sql = substr($Sql, 0, $groupByIndex);
        }

        //*** OrderByClause
        $jsGrid->OrderByClause = ($_POST["sortField"] != null) ? $_POST["sortField"] . "  " . $_POST["sortOrder"] : $jsGrid->OrderByClause;
        $SqlOrderby = "";
        $orderByIndex = strrpos(strtolower($Sql), "order by");
        if ($orderByIndex !== false) {
            $SqlOrderby = substr($Sql, $orderByIndex);
            $Sql = substr($Sql, 0, $orderByIndex);
        }

        //*** WhereByClause
        if ($jsGrid->IsSelectItem) {
            $jsGrid->selectDataItem = $_POST["selectDataItem"];
            if ($jsGrid->selectDataItem) {
                $Sql = $Sql . ($jsGrid->selectDataItem != null ? ((strrpos(strtolower($Sql), "where") === true) ? " AND " : " WHERE ")
                    . "  " . $jsGrid->selectDataColumn . " IN ( " . $jsGrid->selectDataItem . ")" : "");
            }
        }
        $Sql = $Sql . ($jsGrid->WhereByClause != null ? ((strrpos(strtolower($Sql), "where") > -1) ? " AND " : " WHERE ") . $jsGrid->WhereByClause : "");

        $SqlBuilder->Append($Sql);
        //**** NotShowInitialData
        if ($jsGrid->NotShowInitialData == true && $jsGrid->Event) {
            $jsGrid->Event = "Initial";
            if (strrpos(strtolower($Sql), " where ") === false) {
                $SqlBuilder->Append(" WHERE 0=1");
            } else {
                $SqlBuilder->Append(" AND 0=1");
            }
        }

        if ($_POST["formData"] != null) {
            //*** Where By $Sql
            foreach ($formDataObject as $key => $value) {
                //{
                $fieldName = $key;
                $value = $value;
                $isSearch = true;
                $list_SkipParameters = explode(',', $jsGrid->SkipParameters);
                for ($i = 0; $i < count($list_SkipParameters); $i++) {
                    if (strtolower($fieldName) == trim(strtolower($list_SkipParameters[$i])))
                        $isSearch = false;
                }
                if ($isSearch && ($value != null)) {
                    setConditionSQL($SqlBuilder, $fieldName, $value, $jsGrid);
                }
            }

            //*** Where Condition Between
            //     if ($jsGrid->SearchBetween != null) {
            //         $mapper = $formDataObject;
            //         //DBService->formatData($mapper, $jsGrid->FormatData, false);
            //         $list_criteria = explode(",\\[", $jsGrid->SearchBetween);
            //         for ($i = 0; $i < count($list_criteria); $i++) {
            //             $criteria = trim($list_criteria[$i]);
            //             $searchType = "date";
            //             if (strrpos(strtolower($criteria)," as ") === true) {

            //                 //searchType = criteria.Substring(criteria.ToLower().IndexOf(" as ") + " as ".Length).Trim();
            //                 //criteria = criteria.Substring(0, criteria.ToLower().IndexOf(" as ")).Trim();

            //                 $searchType = Trim(substr($criteria, strrpos(strtolower($criteria), " as ") + strlen(" as ")));
            //                 $criteria = Trim(substr($criteria, 0, strrpos(strtolower($criteria), " as ")));
            //             }
            //             if (str_starts_with($criteria,"[")) {
            //                 $criteria = substr($criteria, 1);
            //             }
            //             if (str_ends_with($criteria, "]")) {
            //                 $criteria = substr($criteria, 0, strlen($criteria) - 1);
            //             }
            //             $list_Field = explode(',', trim(substr($criteria, 0, strrpos($criteria, ":"))));
            //             $list_NameValue =explode(',', trim(substr($criteria, strrpos($criteria, ":") + 1)));
            //             $nameValue1 = trim($list_NameValue[0]);
            //             $nameValue2 = (strlen($list_NameValue) > 1) ? trim($list_NameValue[1]) : "";

            //             $SqlValue1 = "";
            //             $SqlValue2 = "";
            //             if (($mapper->ContainsKey($nameValue1) &&  $mapper[$nameValue1]) != "" &&
            //                 ($mapper->ContainsKey($nameValue2) && $mapper[$nameValue2]) != ""
            //             ) {
            //                 if (strrpos(strtolower($SqlBuilder->text)," where ") === false) {
            //                     $SqlBuilder->Append(" WHERE ");
            //                 }

            //                 $optionalSearch = ($SqlBuilder->Trim()->ToLower()->EndsWith("where")) ? "" : "AND";
            //                 if ($list_Field->Length == 1) {
            //                     $SqlValue1 = ($list_Field[0]->ToLower()->IndexOf("date") > -1) ? "CONVERT( ".$searchType .",' ".$mapper[$nameValue1] ."')" :  $mapper[$nameValue1];
            //                     $SqlValue2 = ($list_Field[0]->ToLower()->IndexOf("date") > -1) ? "CONVERT( ".$searchType .",' ".$mapper[$nameValue2] ."')" :  $mapper[$nameValue2];
            //                     $SqlBuilder->Append("  ".$optionalSearch ."  convert( ".$searchType .", ".$list_Field[0] .",101) BETWEEN  ".$SqlValue1 ." AND  ".$SqlValue2);
            //                 } else {
            //                     $SqlValue1 = ($list_Field[0]->ToLower()->IndexOf("date") > -1) ? "CONVERT( ".$searchType .",' ".$mapper[$nameValue1] ."')" :  $mapper[$nameValue1];
            //                     $SqlValue2 = ($list_Field[1]->ToLower()->IndexOf("date") > -1) ? "CONVERT( ".$searchType .",' ".$mapper[$nameValue2] ."')" :  $mapper[$nameValue2];
            //                     if ($jsGrid->TABLENAME == "VendorDialog") {
            //                         $SqlBuilder->Append("  ".$optionalSearch ."  ".$SqlValue1  ." >=  ".$list_Field[0] ." AND  ".$SqlValue2 ." <=  ".$list_Field[1]);
            //                     } else {
            //                         $SqlBuilder->Append("  ".$optionalSearch ."  ".$list_Field[0] ." >=  ".$SqlValue1 ." AND  ".$list_Field[1] ." <=  ".$SqlValue2);
            //                     }
            //                 }
            //             } else if ($mapper->ContainsKey($nameValue1) &&  $StringManager->getIsNULLtoEmpty($mapper[$nameValue1]) != "") {
            //                 if ($SqlBuilder->ToLower()->IndexOf(" where ") == -1) {
            //                     $SqlBuilder->Append(" WHERE ");
            //                 }
            //                 $optionalSearch = ($SqlBuilder->Trim()->ToLower()->EndsWith("where")) ? "" : "AND";
            //                 $SqlValue1 = ($list_Field[0]->ToLower()->IndexOf("date") > -1) ? "CONVERT( ".$searchType .",' ".$mapper[$nameValue1] ."')" : mapper[$nameValue1];
            //                 $SqlBuilder->Append("  ".$optionalSearch ." convert( ".$searchType .", ".$list_Field[0] .",101) =  ".$SqlValue1);
            //             } else if ($mapper->ContainsKey($nameValue2) &&  $StringManager->getIsNULLtoEmpty($mapper[$nameValue2]) != "") {
            //                 if ($SqlBuilder->ToLower()->IndexOf(" where ") == -1) {
            //                     $SqlBuilder->Append(" WHERE ");
            //                 }
            //                 $optionalSearch =  ($SqlBuilder->Trim()->ToLower()->EndsWith("where")) ? "" : "AND";
            //                 if ($list_Field->Length == 1) {
            //                     $SqlValue2 = ($list_Field[0]->ToLower()->IndexOf("date") > -1) ? "CONVERT( ".$searchType .",' ".$mapper[$nameValue2] ."')" :  $mapper[$nameValue2];
            //                     $SqlBuilder->Append("  ".$optionalSearch ." convert( ".$searchType .", ".$list_Field[0] .",101) =  ".$SqlValue2);
            //                 } else {
            //                     $SqlValue2 = ($list_Field[0]->ToLower()->IndexOf("date") > -1) ? "CONVERT( ".$searchType .",' ".$mapper[$nameValue2] ."')" :  $mapper[$nameValue2];
            //                     $SqlBuilder->Append("  ".$optionalSearch ."  convert( ".$searchType .", ".$list_Field[1] .",101) =  ".$SqlValue2);
            //                 }
            //             }
            //         }
            //     }
        }

        //*** Group by
        $SqlBuilder->Append("  " . $SqlGroupby);

        //*** Order by
        if ($_POST["sortField"] == null && $jsGrid->OrderByClause == "") {
            $SqlBuilder->Append(" " . $SqlOrderby);
        } else {
            $SqlBuilder->Append(($jsGrid->OrderByClause != "" ? ((strrpos(strtolower($SqlBuilder->text), "order by") > -1) ? "," : " ORDER BY ") . $jsGrid->OrderByClause : ""));
        }
        $SqlCount .= "( " . ((strrpos(strtolower($SqlBuilder->text), "order by") > -1) ? substr($SqlBuilder->text, 0, strrpos(strtolower($SqlBuilder->text), "order by")) : $SqlBuilder->text) . ")";

        $databaseType = (!$jsGrid->DatabaseType) ? "SQLSERVER" : $jsGrid->DatabaseType;
        $SqlCount .= " AS SqlCount";

        //*** Set Paging
        if ($setPaging) {
            switch ($databaseType) {
                case "SQLSERVER":
                    $offset = 1 + (($page - 1) * $rows);
                    $limit = ($offset->$rows) - 1;
                    $SqlServerDialect = new SqlServerDialect();
                    if ($jsGrid->Sql != null && $jsGrid->Sql != "") {
                        if ($_POST["sortField"] == null) {
                            $Sql = $SqlServerDialect->getLimitString($SqlBuilder->text, $offset, $limit, false);
                        } else {
                            $Sql = $SqlServerDialect->getLimitString($SqlBuilder->text, $offset, $limit, true);
                        }
                    } else {
                        $Sql = $SqlServerDialect->getLimitString($SqlBuilder->text, $offset, $limit, false);
                    }
                    break;
                case  "ORACLE":
                    $offset = (1 + (($page - 1) * $rows)) - 1;
                    $limit = $rows;
                    $OracleDialect = new OracleDialect();
                    $Sql = $OracleDialect->getLimitString($SqlBuilder->text, $offset, $limit);
                    break;
                case  "MYSQL":
                    $offset = (1 + (($page - 1) * $rows)) - 1;
                    $limit = $rows;
                    $MySQLDialect = new MySQLDialect();
                    $Sql = $MySQLDialect->getLimitString($SqlBuilder->text, $offset, $limit);
                    break;
            }
        } else {
            $Sql = $SqlBuilder->text;
        }
        return $array = ["Sql" => $Sql, "SqlCount" =>  $SqlCount];
    } catch (Exception $e) {
        //LogManager.Log4.Error(e.Message, e);
        $Sql = "";
        $SqlCount = "";
    }
}


function setConditionSQL($SqlBuilder, $fieldName, $value, $jsGrid)
{
    if ($value != null && $value != "") {
        $val = $value;
        //*** Check SQL Injection **//
        $dbservice = new DBservice();
        $isSQLInjection = $dbservice->CheckSQLInjection($value);
        if ($isSQLInjection) {
            throw new Exception("*Grid --> SQL Injection Parameter: " . $fieldName . " = " . $val . "");
        }

        if (strrpos(strtolower($SqlBuilder->text), " where ") === false) {
            $SqlBuilder->Append(" WHERE ");
        }
        $optionalSearch = endsWith(trim(strtolower($SqlBuilder->text)), "where") ? "" : "AND";
        $fieldNameWhere = $fieldName;


        //*** Overide Search
        if ($jsGrid->SearchEqual != null && contains(strtolower($jsGrid->SearchEqual), strtolower($fieldName))) {
            $optionalSearch = ($optionalSearch != "" && strrpos(strtolower($jsGrid->SearchEqual), "or " . strtolower($fieldName)) > -1) ? "OR" : $optionalSearch;
            if (gettype($value) == "string") {
                if (contains(strtolower($jsGrid->SearchEqual), "(" . strtolower($fieldName)) || contains(strtolower($jsGrid->SearchEqual), "( " . strtolower($fieldName))) {
                    $SqlBuilder->Append(" " . $optionalSearch . " (" . $fieldNameWhere . " = '" . $val . "'");
                } else if (contains(strtolower($jsGrid->SearchEqual), strtolower($fieldName) . ")") || contains(strtolower($jsGrid->SearchEqual), strtolower($fieldName) . " )")) {
                    $SqlBuilder->Append(" " . $optionalSearch . " " . $fieldNameWhere . " = '" . $val . "')");
                } else {
                    $SqlBuilder->Append(" " . $optionalSearch . " " . $fieldNameWhere . " = '" . $val . "'");
                }
            } else if ($value != 0) {
                $SqlBuilder->Append(" " . $optionalSearch . " " . $fieldNameWhere . " = " . $val . "");
            }
        } else if ($jsGrid->SearchLikeLeft != null && contains(strtolower($jsGrid->SearchLikeLeft), strtolower($fieldName))) {
            $val = (strrpos($val, "%") > -1) ? trim($val) : (trim($val) . "%");
            $optionalSearch = ($optionalSearch != "" && strrpos(strtolower($jsGrid->SearchLikeLeft), "or " . strtolower($fieldName)) > -1) ? "OR" : $optionalSearch;
            $SqlBuilder->Append(" " . $optionalSearch . " " . $fieldNameWhere . " LIKE '" . $val . "'");
        } else if ($jsGrid->SearchLikeRight != null && contains(strtolower($jsGrid->SearchLikeRight), strtolower($fieldName))) {
            $val = (strrpos($val, "%") > -1) ? trim($val) : ("%" . trim($val));
            $optionalSearch = ($optionalSearch != "" && strrpos(strtolower($jsGrid->SearchLikeRight), "or " . strtolower($fieldName)) > -1) ? "OR" : $optionalSearch;
            $SqlBuilder->Append(" " . $optionalSearch . " " . $fieldNameWhere . " LIKE '" . $val . "'");
        } else if ($jsGrid->SearchLike != null && contains(strtolower($jsGrid->SearchLike), strtolower($fieldName))) {
            $val = (strrpos($val, "%") > -1) ? trim($val) : ("%" . trim($val) . "%");
            $optionalSearch = ($optionalSearch != "" && strrpos(strtolower($jsGrid->SearchLike), "or " . strtolower($fieldName)) > -1) ? "OR" : $optionalSearch;
            if (contains(strtolower($jsGrid->SearchLike), "(" . strtolower($fieldName)) || contains(strtolower($jsGrid->SearchLike), "( " . strtolower($fieldName))) {
                $SqlBuilder->Append(" " . $optionalSearch . " (" . $fieldNameWhere . " LIKE '" . $val . "'");
            } else if (contains(strtolower($jsGrid->SearchLike), strtolower($fieldName) . ")") || contains(strtolower($jsGrid->SearchLike), strtolower($fieldName) . " )")) {
                $SqlBuilder->Append(" " . $optionalSearch . " " . $fieldNameWhere . " LIKE '" . $val . "')");
            } else {
                $SqlBuilder->Append(" " . $optionalSearch . " " . $fieldNameWhere . " LIKE '" . $val . "'");
            }
        } else if ($jsGrid->SearchLike != null && contains(strtolower($jsGrid->SearchBetween), strtolower($fieldName))) {
            //*** Skip
        } else if (gettype($value) == "string") {
            $SqlBuilder->Append(" " . $optionalSearch . " " . $fieldNameWhere . " = '" . $val . "'");
        } else if ((gettype($value) == "integer" || gettype($value) == "double") &&  intval($value) != 0) {
            $SqlBuilder->Append(" " . $optionalSearch . " " . $fieldNameWhere . " = " . $val . "");
        }
    }
}

//** Test */
// $_POST = array(
//     'jsGrid' => '{\"jsGrid\":{\"URL\":\"wp-content/plugins/debt-collection/includes/jsGrid/jsGridControlleService.php\",\"TABLENAME\":\"wp_dept_mscustomer\",\"GridNAME\":null,\"IdJsGrid\":\"IdJsGridwp_dept_mscustomer\",\"jsGridControlId\":\"587b16fc99f8d7fc50ab18bb5d1e421b\",\"jsGridControlType\":null,\"IdPagerJsGrid\":\"externalPagerJsGridwp_dept_mscustomer\",\"DatabaseType\":\"MYSQL\",\"SCHEMA\":null,\"DATABASE\":null,\"Namespace\":null,\"PKName\":null,\"Sql\":\"SELECT wp_dept_mscustomer.* FROM wp_dept_mscustomer ORDER BY fname \",\"WhereByClause\":null,\"OrderByClause\":null,\"FormSearchId\":\"formSearch\",\"ButtonSearchId\":\"btnSearch\",\"ButtonExportId\":\"btnExport\",\"ButtonExportPDFId\":null,\"SearchEqual\":null,\"SearchLike\":\"idcard, customerCode\",\"SearchLikeRight\":null,\"SearchBetween\":null,\"FormatData\":\"createDate:=Y-m-d H:i:s->d/m/Y H:i, updateDate:=Y-m-d H:i:s->d/m/Y H:i\",\"theme\":null,\"CustomPath\":null,\"CustomParameters\":null,\"SkipHTMLParameters\":null,\"SkipParameters\":null,\"pageSize\":20,\"height\":\"1000px\",\"width\":\"100%\",\"filtering\":null,\"sorting\":null,\"paging\":null,\"pageButtonCount\":null,\"select\":null,\"rowClick\":null,\"rowRenderer\":null,\"updateItem\":null,\"deleteItem\":null,\"onSearch\":null,\"onButtonSearch\":null,\"onButtonExport\":null,\"onButtonExportPDF\":null,\"onItemEditing\":\"onItemEditing\",\"onItemDeleting\":\"onItemDeleting\",\"loadDataDone\":null,\"IsSelectItem\":null,\"IsSelectMultiPage\":null,\"selectDataItem\":null,\"selectDataColumn\":null,\"ReportSource\":null,\"DataSet\":null,\"Parameters\":null,\"Attributes\":null,\"style\":null,\"NotShowInitialData\":null,\"Event\":null}}',
//     'jsGridControl' => '{\"jsGridControlId\":\"5c626d97ee67489b19d989527ce32f7c\"}',
//     'pageIndex' => '1',
//     'pageSize' => '20',
//     //'formData' => []
//     'formData' => '{\"idcard\":\"\",\"customerCode\":\"%3\",\"fname\":\"\",\"lname\":\"\",\"email\":\"\",\"phone\":\"\"}',
//     //'formData' => '{\"idcard\":\"001\",\"customerCode\":\"1\",\"fname\":\"\",\"lname\":\"\",\"email\":\"\",\"phone\":\"\"}' 
//     'sortField' => 'idcard', 
//     'sortOrder' => 'asc'
// );

//var_dump($_POST);
Get();
