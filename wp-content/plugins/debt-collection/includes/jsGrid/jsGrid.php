<?php

/**
 * 
 	  @author BeerSting <br>
 * <b>The MIT License (MIT) Copyright: </b><br>
 * Copyright (c) 2022, BeerSting<br>
 * 
 * <b>Create by: </b><br>
 * Yoottapong Wongwiwut<br>  
 * 
 * <b>Create Date: </b><br>
 *  Jan 07 2022<br>
 * 
 * <b>Email: </b><br>
 * <A href="mailto:beer.sting@gmail.com">beer.sting@gmail.com</A><br> 
	  @version 1.0
 * 
 */

class jsGrid
{
    //**** Main
    public $URL = "wp-content/plugins/debt-collection/includes/jsGrid/jsGridControlleService.php";

    public $TABLENAME;

    public $GridNAME;

    public $IdJsGrid;

    public $jsGridControlId;

    public $jsGridControlType;

    public $IdPagerJsGrid;

    public $DatabaseType = "MYSQL";

    public $SCHEMA;

    public $DATABASE;

    //private string _namespace;
    public $Namespace;
    // {
    //     get
    //     {
    //         return string.IsNullOrEmpty(_namespace)  $"MvcWebmedic.Models" : _namespace;
    //     }

    //     set { _namespace = value; }
    // }

    public $PKName;
    //public $AliasNameWhere;
    public $Sql;
    //public IDictionary<string, string> Sqls = new Dictionary<string, string>();    
    public $WhereByClause;
    public $OrderByClause;
    public $FormSearchId;
    public $ButtonSearchId;
    public $ButtonExportId;
    public $ButtonExportPDFId;
    public $SearchEqual;
    public $SearchLike;
    public $SearchLikeLeft;
    public $SearchLikeRight;
    public $SearchBetween;

    //**** Custom
    public $FormatData;
    public $fields;
    public $theme;
    public $CustomPath;
    public $CustomParameters;
    public $SkipHTMLParameters;
    public $SkipParameters = "PK, action";
    // {
    //     get { return "__RequestVerificationToken, selectDataItem, " + SkipHTMLParameters; }
    // }

    //**** Property
    public $pageSize; // int
    public $height;
    public $width;
    public $filtering; // bool
    public $sorting; // bool
    public $paging; // bool
    public $pageButtonCount; // int

    //**** Event
    public $select;
    public $rowClick;
    public $rowRenderer;
    public $updateItem;
    public $deleteItem;
    public $onSearch;
    public $onButtonSearch;
    public $onButtonExport;
    public $onButtonExportPDF;
    public $onItemEditing = "onItemEditing";
    public $onItemDeleting = "onItemDeleting";
    public $loadDataDone;

    //**** Control
    public $IsSelectItem;  // bool
    public $IsSelectMultiPage;  // // Not This Imprement Funtion
    public $selectDataItem;
    //public TypeVariable $selectDataType;
    public $selectDataColumn;

    //**** Report
    public $ReportSource;
    //public $ButtonSearchReportId;
    public $DataSet;
    //public IDictionary<string, string> $DataSetSQL;
    public $Parameters;  // Object
    public $Attributes;  // Object
    public $style;
    public $NotShowInitialData; // bool
    public $Event;
}
