<?php 
$document_root = str_replace("\\", "/", explode("wp-content", dirname(__FILE__))[0]);
require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGrid.php');
require_once($document_root . 'wp-content/plugins/debt-collection/includes/common-function.php');

/**
 * 
 	  @author BeerSting <br>
 * <b>The MIT License (MIT) Copyright: </b><br>
 * Copyright (c) 2022, BeerSting<br>
 * 
 * <b>Create by: </b><br>
 * Yoottapong Wongwiwut<br>  
 * 
 * <b>Create Date: </b><br>
 *  Jan 07 2022<br>
 * 
 * <b>Email: </b><br>
 * <A href="mailto:beer.sting@gmail.com">beer.sting@gmail.com</A><br> 
	  @version 1.0
 * 
 */


class jsGridControllerBase
{
    public $jsGrid;
    private function GenerateControlId($jsGrid)
    {
        $jsGrid->jsGridControlId = bin2hex(openssl_random_pseudo_bytes(16));
        $_SESSION[$jsGrid->jsGridControlId] = $this->jsGrid;
    }

    function Create($mJsGrid)
    {
        $this->jsGrid = $mJsGrid;
        if ($this->jsGrIdJsGridControlId == null) {
            $this->GenerateControlId($this->jsGrid);
        }
        
        $this->jsGrid->IdJsGrid = (!$this->jsGrid->GridNAME) ? "IdJsGrid" . $this->jsGrid->TABLENAME : "IdJsGrid" . $this->jsGrid->GridNAME;
        $this->jsGrid->IdPagerJsGrid = (!$this->jsGrid->GridNAME) ? "externalPagerJsGrid" . $this->jsGrid->TABLENAME : "externalPagerJsGrid" . $this->jsGrid->GridNAME;
        $htmlString = "<div id=\"" .$this->jsGrid->IdJsGrid ."\" style=\"" .$this->jsGrid->style ."\"></div>\n<div id=\"" .$this->jsGrid->IdPagerJsGrid ."\" class=\"external-pager\" align=\"center\"></div>";
        $jsString = new StringBuilder();
        $jsString->Append("\n<script type=\"text/javascript\">");

        $jsGridFields = $this->jsGrid->fields;
        $jsString->Append("\n var " .$this->jsGrid->IdJsGrid ."={\"jsGridControlId\":\"" .$this->jsGrid->jsGridControlId ."\"};");
        $jsGridModel = clone $this->jsGrid;
        unset($jsGridModel->fields);
        $jsString->Append("\n var jsGrid_" .$this->jsGrid->jsGridControlId ."={\"jsGrid\":" .json_encode($jsGridModel) ."};");
        $jsString->Append("\n");

        //*** Event Selection Item
        if ($this->jsGrid->IsSelectItem) {
            $jsString->Append("\n var isSelect" .$this->jsGrid->IdJsGrid ." = true;");
            $jsString->Append("\n var selectData" .$this->jsGrid->IdJsGrid ." = [];");
            $jsString->Append("\n var selectDataItem" .$this->jsGrid->IdJsGrid .";");
            $jsString->Append("\n function selectItem" .$this->jsGrid->IdJsGrid ."(control, isSelect)");
            $jsString->Append("\n {");
            $jsString->Append("\n   selectData" .$this->jsGrid->IdJsGrid ." = [];");
            $jsString->Append("\n   selectDataItem" .$this->jsGrid->IdJsGrid ."= null;");
            $jsString->Append("\n   if(isSelect == false || isSelect == true) isSelect" .$this->jsGrid->IdJsGrid ." = isSelect;");
            $jsString->Append("\n   if(isSelect" .$this->jsGrid->IdJsGrid ." == false){ return;}");
            $jsString->Append("\n   var items = $('#" .$this->jsGrid->IdJsGrid ."').jsGrid(\"option\", \"data\");");
            $jsString->Append("\n    for (i = 0; i < items.length; i++) {");
            $jsString->Append("\n         if (items[i].Checked) {");
            $jsString->Append("\n             if (items[i].Checked == true) {");
            $jsString->Append("\n                 selectData" .$this->jsGrid->IdJsGrid .".push(items[i]);");
            if ($this->jsGrid->selectDataType == "Integer") {
                $jsString->Append("\n                 selectDataItem" .$this->jsGrid->IdJsGrid ." = ((selectDataItem" .$this->jsGrid->IdJsGrid .") ? selectDataItem" .$this->jsGrid->IdJsGrid ." .\",\" : \"\") .items[i]." .$this->jsGrid->selectDataColumn .";");
            } else {
                $jsString->Append("\n                 selectDataItem" .$this->jsGrid->IdJsGrid ." = ((selectDataItem" .$this->jsGrid->IdJsGrid .") ? selectDataItem" .$this->jsGrid->IdJsGrid ." .\",\" : \"\") .\"'\" .items[i]." .$this->jsGrid->selectDataColumn ." .\"'\";");
            }
            $jsString->Append("\n             }");
            $jsString->Append("\n         }");
            $jsString->Append("\n      }");
            $jsString->Append("\n }");
            $jsString->Append("\n");
        }

        
        //*** Event Click Search
        if ($this->jsGrid->ButtonSearchId != null) {
            $jsString->Append("\n $(document).ready(function () {");
            $jsString->Append("\n jQuery(\"#" .$this->jsGrid->ButtonSearchId ."\").click(function() {");
            $jsString->Append("\n  if(event){");
            $jsString->Append("\n     event.stopPropagation();");
            $jsString->Append("\n     event.preventDefault();");
            $jsString->Append("\n  }");
            if ($this->jsGrid->IsSelectItem) {
                $jsString->Append("\n   selectData" .$this->jsGrid->IdJsGrid ." = [];");
                $jsString->Append("\n   selectDataItem" .$this->jsGrid->IdJsGrid ."= null;");
            }
            if ($this->jsGrid->onButtonSearch != null) {
                $jsString->Append("\n " .$this->jsGrid->onButtonSearch);
            }
            $jsString->Append("\n  jQuery(\"#" .$this->jsGrid->ButtonSearchId ."\").attr(\"disabled\", true);");
            if ($this->jsGrid->FormSearchId)
            {
                $jsString->Append("\n  jQuery(\"#" .$this->jsGrid->IdJsGrid ."\").jsGrid(\"reset\", search_" .$this->jsGrid->IdJsGrid ."(null,jQuery('FORM#" .$this->jsGrid->FormSearchId ."').serializeObject())).done(function () {");
            }else
            {
                $jsString->Append("\n  jQuery(\"#" .$this->jsGrid->IdJsGrid ."\").jsGrid(\"reset\", search_" .$this->jsGrid->IdJsGrid ."(null,null)).done(function () {");

            }
            $jsString->Append("\n  jQuery(\"#" .$this->jsGrid->ButtonSearchId ."\").attr(\"disabled\", false);");
            $jsString->Append("\n   });");
            $jsString->Append("\n });");
            $jsString->Append("\n });");
            $jsString->Append("\n");
        }

        // //*** Event Click Export Excel
        // if ($this->jsGrid->ButtonExportId != null) {
        //     $jsString->Append("\n $(document).ready(function () {");
        //     $jsString->Append("\n jQuery(\"#" .$this->jsGrid->ButtonExportId ."\").click(function() {");
        //     $jsString->Append("\n  if(event){");
        //     $jsString->Append("\n     event.stopPropagation();");
        //     $jsString->Append("\n     event.preventDefault();");
        //     $jsString->Append("\n  }");
        //     if ($this->jsGrid->onButtonExport != null) {
        //         $jsString->Append("\n " .$this->jsGrid->onButtonExport);
        //     }
        //     $jsString->Append("\n export" .$this->jsGrid->IdJsGrid ."(this);");
        //     $jsString->Append("\n });");
        //     $jsString->Append("\n });");
        //     $jsString->Append("\n");
        // }

        // //*** Event Click Export PDF
        // if ($this->jsGrid->ButtonExportPDFId != null) {
        //     $jsString->Append("\n $(document).ready(function () {");
        //     $jsString->Append("\n jQuery(\"#" .$this->jsGrid->ButtonExportPDFId ."\").click(function() {");
        //     $jsString->Append("\n  if(event){");
        //     $jsString->Append("\n     event.stopPropagation();");
        //     $jsString->Append("\n     event.preventDefault();");
        //     $jsString->Append("\n  }");
        //     if ($this->jsGrid->onButtonExportPDF != null) {
        //         $jsString->Append("\n " .$this->jsGrid->onButtonExportPDF);
        //     }
        //     $jsString->Append("\n exportPDF" .$this->jsGrid->IdJsGrid ."(this);");
        //     $jsString->Append("\n });");
        //     $jsString->Append("\n });");
        //     $jsString->Append("\n");
        // }

        //*** Function Manage Data
        $jsString->Append("\n var json" .$this->jsGrid->IdJsGrid ." = { jsGridControl:JSON.stringify(" .$this->jsGrid->IdJsGrid .")};");
        $jsString->Append("\n var json" .("jsGrid_" .$this->jsGrid->jsGridControlId) ." = { jsGrid:JSON.stringify(" .("jsGrid_" .$this->jsGrid->jsGridControlId) .")};");
        $jsString->Append("\n var data" .$this->jsGrid->IdJsGrid .";");
        if ($this->jsGrid->onSearch != null) {
            $jsString->Append("\n var " .$this->jsGrid->onSearch .";");
        }

        //*** Function Search
        $jsString->Append("\n function search_" .$this->jsGrid->IdJsGrid ."(jsFilter,formData)");
        $jsString->Append("\n {");
        if ($this->jsGrid->onSearch != null) {
            $jsString->Append("\n " .(($this->jsGrid->onSearch != null) ? "if(" .$this->jsGrid->onSearch ."){ formData=" .$this->jsGrid->onSearch ."(jsFilter,formData);}" : "") ."");
        }
        $jsString->Append("\n data" .$this->jsGrid->IdJsGrid ." = jQuery.extend(json" .$this->jsGrid->IdJsGrid .",jsFilter); ");
        $jsString->Append("\n data" .$this->jsGrid->IdJsGrid ." = jQuery.extend(json" .("jsGrid_" .$this->jsGrid->jsGridControlId) .",data" .$this->jsGrid->IdJsGrid ."); ");
        $jsString->Append("\n if(formData){");
        $jsString->Append("\n          data" .$this->jsGrid->IdJsGrid .".formData = JSON.stringify(formData);");
        //$jsString->Append("\n          data =  jQuery.extend(data,formData);");
        $jsString->Append("\n     if(formData.__RequestVerificationToken){");
        $jsString->Append("\n          data" .$this->jsGrid->IdJsGrid .".__RequestVerificationToken=formData.__RequestVerificationToken;");
        $jsString->Append("\n     }else{");
        $jsString->Append("\n          data" .$this->jsGrid->IdJsGrid .".__RequestVerificationToken=$(\"[name = __RequestVerificationToken]\").val();");
        $jsString->Append("\n     }");
        $jsString->Append("\n }else{");
        $jsString->Append("\n          data" .$this->jsGrid->IdJsGrid .".__RequestVerificationToken=$(\"[name = __RequestVerificationToken]\").val();");
        $jsString->Append("\n }");
        $jsString->Append("\n          return data" .$this->jsGrid->IdJsGrid .";");
        $jsString->Append("\n }");
        $jsString->Append("\n");

        // //*** Function Export Excel
        // if ($this->jsGrid->ButtonExportId != null)
        // {
        //     $jsString->Append("\n function export" .$this->jsGrid->IdJsGrid ."(control)");
        //     $jsString->Append("\n {");
        //     if (!String.IsNullOrEmpty($this->jsGrid->FormSearchId))
        //     {
        //         $jsString->Append("\n   var data = search_" .$this->jsGrid->IdJsGrid ."(null,jQuery('FORM#" .$this->jsGrid->FormSearchId ."').serializeObject());");
        //     }
        //     else
        //     {
        //         $jsString->Append("\n   var data = search_" .$this->jsGrid->IdJsGrid ."(null,null);");

        //     }
        //     $jsString->Append("\n var data = jQuery.extend({}, data)");
        //     if ($this->jsGrid->IsSelectItem)
        //     {
        //         $jsString->Append("\n data.selectDataItem = selectDataItem" .$this->jsGrid->IdJsGrid .";");
        //     }
        //     $jsString->Append("\n   data.pageIndex = 1;");
        //     $jsString->Append("\n   data.pageSize = 99999999;");
        //     $jsString->Append("\n   $(\"#overlay\").show();");
        //     $jsString->Append("\n   ajax_execute(\"../" .$this->jsGrid->URL ."/Export" ."\", data, function success(ajaxResult) {");
        //     $jsString->Append("\n    $(\"#overlay\").hide();");
        //     $jsString->Append("\n    if(ajaxResult.status == \"Success\"){");
        //     $jsString->Append("\n       toastr[\"success\"](ajaxResult.text);");
        //     $jsString->Append("\n       window.open('../' .ajaxResult.value, 'Download');");
        //     //$jsString->Append("\n       window.open('../" .$this->jsGrid->URL ."/Open?file=' .ajaxResult.value, 'Download');");
        //     $jsString->Append("\n    }");
        //     $jsString->Append("\n  },function error(ajaxResult) {");
        //     $jsString->Append("\n   $(\"#overlay\").hide();");
        //     $jsString->Append("\n   showDialog(\"Please Check Error.\", ajaxResult.text, Close, \"red\");");
        //     $jsString->Append("\n  }, true);");
        //     $jsString->Append("\n }");
        //     $jsString->Append("\n");
        // }

        //  //*** Function Export PDF
        // if ($this->jsGrid->ButtonExportPDFId != null)
        // {
        //     $jsString->Append("\n function exportPDF" .$this->jsGrid->IdJsGrid ."(control)");
        //     $jsString->Append("\n {");
        //     if (!String.IsNullOrEmpty($this->jsGrid->FormSearchId))
        //     {
        //         $jsString->Append("\n   var data = search_" .$this->jsGrid->IdJsGrid ."(null,jQuery('FORM#" .$this->jsGrid->FormSearchId ."').serializeObject());");
        //     }
        //     else
        //     {
        //         $jsString->Append("\n   var data = search_" .$this->jsGrid->IdJsGrid ."(null,null);");

        //     }
        //     $jsString->Append("\n var data = jQuery.extend({}, data)");
        //     if ($this->jsGrid->IsSelectItem)
        //     {
        //         $jsString->Append("\n data.selectDataItem = selectDataItem" .$this->jsGrid->IdJsGrid .";");
        //     }
        //     //*** Set Fix Parameter
        //     if ($this->jsGrid->Parameters != null)
        //     {
        //         IDictionary<string, object> mapParameters = DictionaryManager.ToDictionary($this->jsGrid->Parameters);
        //         foreach (KeyValuePair<string, object> parameter in mapParameters)
        //         {
        //             String keyName = parameter.Key;
        //             String value = parameter.Value;
        //             $jsString->Append("\n   data." .keyName ." = '" .SecurityUtil.Encrypt(value) ."';");
        //         }
        //     }
        //     $jsString->Append("\n   data.pageIndex = 1;");
        //     $jsString->Append("\n   data.pageSize = 99999999;");
        //     $jsString->Append("\n   $(\"#overlay\").show();");
        //     $jsString->Append("\n   ajax_execute(\"../" .$this->jsGrid->URL ."/ExportPDF" ."\", data, function success(ajaxResult) {");
        //     $jsString->Append("\n    $(\"#overlay\").hide();");
        //     $jsString->Append("\n    if(ajaxResult.status == \"Success\"){");
        //     $jsString->Append("\n       toastr[\"success\"](ajaxResult.text);");
        //     //$jsString->Append("\n       window.open('../' .ajaxResult.value, 'Download');");
        //     $jsString->Append("\n       window.open('../" .$this->jsGrid->URL ."/Open?file=' .ajaxResult.value, 'Download');");
        //     $jsString->Append("\n    }");
        //     $jsString->Append("\n  },function error(ajaxResult) {");
        //     $jsString->Append("\n   $(\"#overlay\").hide();");
        //     $jsString->Append("\n   showDialog(\"Please Check Error.\", ajaxResult.text, Close, \"red\");");
        //     $jsString->Append("\n  }, true);");
        //     $jsString->Append("\n }");
        //     $jsString->Append("\n");
        // }

        //*** Function Create Grid
        $jsString->Append("\n                $(\"#" .$this->jsGrid->IdJsGrid ."\").jsGrid({");
        $jsString->Append("\n                    height: \"" .(($this->jsGrid->height != null) ? $this->jsGrid->height : "440px") ."\",");
        $jsString->Append("\n                    width: \"" .(($this->jsGrid->width != null) ? $this->jsGrid->width : "100%") ."\",");
        $jsString->Append("\n                    filtering: " .(($this->jsGrid->filtering != null) ? $this->jsGrid->filtering->ToLower() : "false") .",");
        $jsString->Append("\n                    editing: true,");
        $jsString->Append("\n                    inserting: false,");
        $jsString->Append("\n                    confirmDeleting: false,");
        $jsString->Append("\n                    sorting: " .(($this->jsGrid->sorting != null) ? $this->jsGrid->sorting->ToLower() : "true") .",");
        $jsString->Append("\n                    paging: " .(($this->jsGrid->paging != null) ? $this->jsGrid->paging->ToLower() : "true") .",");
        $jsString->Append("\n                    autoload: true,");
        $jsString->Append("\n                    pageLoading: true,");
        $jsString->Append("\n                    pageSize: " .(($this->jsGrid->pageSize != null) ? $this->jsGrid->pageSize : "10") .",");
        $jsString->Append("\n                    pageButtonCount: " .(($this->jsGrid->pageButtonCount != null) ? $this->jsGrid->pageButtonCount : "5") .",");
        //$jsString->Append("                    deleteConfirm: "Do you really want to delete the client?",");
        //$jsString->Append("                    //controller: db,");
        //$jsString->Append("\n                    pagerContainer: \"#" .$this->jsGrid->IdPagerJsGrid ."\",");
        //$jsString->Append("\n                    pagerFormat: \"( {pageIndex} of {pageCount} )&nbsp;&nbsp; {first} {prev} {pages} {next} {last} &nbsp;&nbsp;\",");
        //$jsString->Append("\n                    pagePrevText: \"<\",");
        //$jsString->Append("\n                    pageNextText: \">\",");
        //$jsString->Append("\n                    pageFirstText: \"<<\",");
        //$jsString->Append("\n                    pageLastText: \">>\",");
        //$jsString->Append("\n                    pageNavigatorNextText: \"&#8230;\",");
        //$jsString->Append("\n                    pageNavigatorPrevText: \"&#8230;\",");
        $jsString->Append("\n                    controller: {");
        $jsString->Append("\n                        loadData: function (filter) {");
        $jsString->Append("\n                            var deferred = $.Deferred();");
        $jsString->Append("\n                            return $.ajax({");
        $jsString->Append("\n                                type: \"POST\",");
        $jsString->Append("\n                                url: \"../" .$this->jsGrid->URL ."\",");
        $jsString->Append("\n                                dataType: \"json\",");
        if ($this->jsGrid->FormSearchId)
        {
             $jsString->Append("\n                                data: search_" .$this->jsGrid->IdJsGrid ."(filter,jQuery('FORM#" .$this->jsGrid->FormSearchId ."').serializeObject()),");
        }
        else
        {
            $jsString->Append("\n                                data: search_" .$this->jsGrid->IdJsGrid ."(filter,null),");
        }
        $jsString->Append("\n                                error: function (res, status, exeption) {");
        $jsString->Append("\n                                  if(jQuery(\"#" .$this->jsGrid->FormSearchId ." #" .$this->jsGrid->ButtonSearchId ."\"))jQuery(\"#" .$this->jsGrid->FormSearchId ." #" .$this->jsGrid->ButtonSearchId ."\").attr(\"disabled\", false);");
        $jsString->Append("\n                                  toastr[\"error\"](((exeption)?exeption:\"Error\"));");
        $jsString->Append("\n                                },");
        $jsString->Append("\n                            }).done(function (response) {");
        $jsString->Append("\n                                 if(typeof(response)==\"object\"){");
        $jsString->Append("\n                               " .(($this->jsGrid->loadDataDone != null) ? $this->jsGrid->loadDataDone ."(response);" : "") ."");
        $jsString->Append("\n                                 }else{ ");
        $jsString->Append("\n                                     toastr[\"error\"](((response)?response:\"Error!\")); ");
        $jsString->Append("\n                                     if(jQuery(\"#" .$this->jsGrid->ButtonSearchId ."\"))jQuery(\"#" .$this->jsGrid->ButtonSearchId ."\").attr(\"disabled\", false);");
        $jsString->Append("\n                                 }");
        $jsString->Append("\n                            });");
        $jsString->Append("\n                        " .(($this->jsGrid->rowRenderer != null) ? "return deferred.promise();" : "") ."");
        $jsString->Append("\n                        },");
        if ($this->jsGrid->updateItem != null) {
            $jsString->Append("\n                        updateItem: function (item) {");
            $jsString->Append("\n                        " .(($this->jsGrid->updateItem != null) ? $this->jsGrid->updateItem ."(item);" : "") ."");
            $jsString->Append("\n                        },");
        }
        if ($this->jsGrid->deleteItem != null) {
            $jsString->Append("\n                        deleteItem: function (item) {");
            $jsString->Append("\n                        " .(($this->jsGrid->deleteItem != null) ? $this->jsGrid->deleteItem ."(item);" : "") ."");
            $jsString->Append("\n                        },");
        }
        $jsString->Append("\n                    },");

        $jsString->Append("\n                    onItemEditing:function(args){");
        $jsString->Append("\n                          args.cancel = true;");
        $jsString->Append("\n                        " .(($this->jsGrid->onItemEditing != null) ? $this->jsGrid->onItemEditing ."(args);" : "") ."");
        $jsString->Append("\n                    },");

        $jsString->Append("\n                    onItemDeleting:function(args){");
        $jsString->Append("\n                          args.cancel = true;");
        $jsString->Append("\n                        " .(($this->jsGrid->onItemDeleting != null) ? $this->jsGrid->onItemDeleting ."(args);" : "") ."");
        $jsString->Append("\n                    },");

        if ($this->jsGrid->select != null) {
            $jsString->Append("\n                    select:function(dv, record, index, eOpts){");
            $jsString->Append("\n                        " .(($this->jsGrid->select != null) ? $this->jsGrid->select ."(dv, record, index, eOpts);" : "") ."");
            $jsString->Append("\n                    },");
        }

        $jsString->Append("\n                    rowClick: function (row) {");
        $jsString->Append("\n                          args.cancel = true;");
        $jsString->Append("\n                          " .(($this->jsGrid->rowClick != null) ? $this->jsGrid->rowClick ."(row);" : ""));
        $jsString->Append("\n                    },");

        if ($this->jsGrid->rowRenderer != null) {
            $jsString->Append("\n                    rowRenderer: function(item){");
            $jsString->Append("\n                        " .(($this->jsGrid->rowRenderer != null) ? "return " .$this->jsGrid->rowRenderer ."(item);" : "") ."");
            $jsString->Append("\n                    },");
        }
        $jsString->Append("\n                   fields: [");
        $jsString->Append("\n                        " .(($jsGridFields != null) ? $jsGridFields : ""));
        $jsString->Append("\n                    ]");
        $jsString->Append("\n                });");
        $jsString->Append("\n");
        $jsString->Append("\n</script>");
           
        return ($htmlString .$jsString->text);
    }
}
