<?php
$document_root = str_replace("\\", "/", explode("wp-content", dirname(__FILE__))[0]);
require_once($document_root . 'wp-config.php');

/**
 * 
 	  @author BeerSting <br>
 * <b>The MIT License (MIT) Copyright: </b><br>
 * Copyright (c) 2022, BeerSting<br>
 * 
 * <b>Create by: </b><br>
 * Yoottapong Wongwiwut<br>  
 * 
 * <b>Create Date: </b><br>
 *  Jan 07 2022<br>
 * 
 * <b>Email: </b><br>
 * <A href="mailto:beer.sting@gmail.com">beer.sting@gmail.com</A><br> 
	  @version 1.0
 * 
 */

class DBservice
{
    public $sqlCheckList = [
        "--", ";--", ";", "/*", "*/", "@@", " @", "char", "nchar", "varchar", "nvarchar", "alter ", "begin", "cast", "create ", "cursor", "declare", "delete ", "drop ", " end", "exec", "execute", "fetch", "insert ", "kill", "select ", "sys ", "sysobjects", "syscolumns", "table ", "update ", "where ", "and ", "or ", "=", "is ", "from ", ";", "'"
    ];

    public function CheckSQLInjection($value)
    {
        $isSQLInjection = false;
        for ($i = 0; $i <= count($this->sqlCheckList) - 1; $i++) {
            if (strrpos(strtolower($value), strtolower($this->sqlCheckList[$i])) > -1) {
                $isSQLInjection = true;
            }
        }
        return $isSQLInjection;
    }

    public function getListValue($sql, $formatData = "")
    {
        $listData = $this->getListValueFromSQL($sql);
        if ($formatData) {
            for ($i = 0; $i < count($listData); $i++) {
                $this->formatData($listData[$i], $formatData);
            }
        }
        return $listData;
    }

    public function getListValueFromSQL($sql)
    {
        global $wpdb;
        $listData = $wpdb->get_results($sql);
        return $listData;
    }

    public function formatData($mapper, $format)
    {
        $this->formatDataMapping($mapper, $format, true);
    }

    public function  formatDataMapping($mapper, string $format, bool $isSourceToTarget)
    {
        if ($format == null) return;
        $data_format = explode(',', str_replace('\n', '', $format));
        for ($i = 0; $i < count($data_format); $i++) {
            $chk = explode(":=", trim($data_format[$i]));
            $name = trim($chk[0]);
            $isMap = false;
            if (property_exists($mapper, $name)) {
                $isMap = true;
            }
            if ($isMap == false && $mapper->$name) {
                $isMap = true;
                $name = $name;
            }
            if ($isMap) {
                $map_format = explode('&', $chk[1]);
                for ($j = 0; $j < count($map_format); $j++) {
                    $str_format = explode("->", $map_format[$j]);
                    $start = null;
                    $new_format = null;
                    if ($isSourceToTarget == false) {
                        $start = $str_format[1];
                        $new_format = $str_format[0];
                    } else {
                        $start = $str_format[0];
                        $new_format = $str_format[1];
                    }
                    if ($mapper->$name != null && (trim($mapper->$name) != "")) {
                        if ($mapper->$name instanceof DateTime) {
                            $mapper->$name = date($new_format, $mapper->$name);
                        //} else if (strrpos(strtolower($name), "date") > -1) {
                        } else {    
                            // if (strlen($mapper->$name) != strlen($start)) {
                            //     $start = substr($start, 0, strlen($mapper->$name));
                            // }
                            $mapper->$name =  strtotime($mapper->$name);
                            $mapper->$name = date($new_format, $mapper->$name);
                        } 
                        // else {
                        //     if (trim($mapper->$name) == $start) $mapper->$name = $new_format;
                        // }
                    }
                }
            }
        }
    }
}
