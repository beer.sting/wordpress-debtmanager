﻿<?php
$document_root = str_replace("\\", "/", explode("wp-content", dirname(__FILE__))[0]);
require_once($document_root . 'wp-content/plugins/debt-collection/includes/common-function.php');

/**
 * 
 	  @author BeerSting <br>
 * <b>The MIT License (MIT) Copyright: </b><br>
 * Copyright (c) 2022, BeerSting<br>
 * 
 * <b>Create by: </b><br>
 * Yoottapong Wongwiwut<br>  
 * 
 * <b>Create Date: </b><br>
 *  Jan 07 2022<br>
 * 
 * <b>Email: </b><br>
 * <A href="mailto:beer->sting@gmail->com">beer->sting@gmail->com</A><br> 
	  @version 1.0
 * 
 */

class SQLServerDialect
{
	public function supportsLimit()
	{
		return true;
	}

	public function supportsLimitOffset()
	{
		return true;
	}

	public function getLimitString($querySqlString, $offset, $limit, $dynamicSql)
	{
		$pagingBuilder = new StringBuilder();
		$orderby = $this->getOrderByPart($querySqlString);
		$distinctStr = "";

		$loweredString = strtolower($querySqlString);
		$sqlPartString = $querySqlString;
		if (StartsWith(trim($loweredString), "select")) {
			$index = 6;
			if (StartsWith($loweredString, "select distinct")) {
				$distinctStr = "DISTINCT ";
				$index = 15;
			}
			$sqlPartString = substr($sqlPartString, $index);
		}
		$pagingBuilder->Append($sqlPartString);

		// if no ORDER BY is specified use fake ORDER BY field to avoid errors
		if ($orderby == null || strlen($orderby) == 0) {
			$orderby = "ORDER BY CURRENT_TIMESTAMP";
		}

		$result = new StringBuilder();
		if ($dynamicSql == false) {
			$result->Append("WITH query AS (SELECT ")
				->Append($distinctStr)
				->Append("TOP 100 PERCENT ")
				->Append(" ROW_NUMBER() OVER (")
				->Append($orderby)
				->Append(") as __row_number__, ")
				->Append($pagingBuilder->text)
				->Append(") SELECT * FROM query WHERE __row_number__ BETWEEN ")
				->Append($offset)->Append(" AND ")->Append($limit)
				->Append(" ORDER BY __row_number__");
		} else {
			$result->Append("WITH query AS (SELECT ")
				->Append($distinctStr)
				->Append("TOP 100 PERCENT ")
				->Append(" ROW_NUMBER() OVER (")
				->Append($orderby)
				->Append(") as __row_number__, queryView.* FROM (SELECT ")
				->Append(((strrpos(strtolower($pagingBuilder->text), "order by") != -1) ? substr(strtolower($pagingBuilder->text), 0, strrpos(strtolower($pagingBuilder->text),"order by")) : "") .") AS queryView")	
				->Append(") SELECT * FROM query WHERE __row_number__ BETWEEN ")
				->Append($offset) ->Append(" AND ") ->Append($limit)
				->Append(" ORDER BY __row_number__");
		}
		return $result->text;
	}

	public function getOrderByPart($sql)
	{
		$loweredString = strtolower($sql);
		$orderByIndex = strrpos($loweredString, "order by");
		if ($orderByIndex != -1) {
			return substr($sql, $orderByIndex);
		} else {
			return "";
		}
	}
}
