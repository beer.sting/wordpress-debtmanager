﻿<?php
$document_root = str_replace("\\", "/", explode("wp-content", dirname(__FILE__))[0]);
require_once($document_root . 'wp-content/plugins/debt-collection/includes/common-function.php');

/**
 * 
 	  @author BeerSting <br>
 * <b>The MIT License (MIT) Copyright: </b><br>
 * Copyright (c) 2022, BeerSting<br>
 * 
 * <b>Create by: </b><br>
 * Yoottapong Wongwiwut<br>  
 * 
 * <b>Create Date: </b><br>
 *  Jan 07 2022<br>
 * 
 * <b>Email: </b><br>
 * <A href="mailto:beer.sting@gmail.com">beer.sting@gmail.com</A><br> 
	  @version 1.0
 * 
 */

class MySQLDialect
{
	public function getLimitString($sql, $offset, $limit)
	{
		$SqlBuilder = new StringBuilder();
		$SqlBuilder->Append($sql)->Append($limit > 0 ? " limit "  .$offset  .","  .$limit : " limit "  .$offset);
		return $SqlBuilder->text;
	}
}

