﻿<?php
$document_root = str_replace("\\", "/", explode("wp-content", dirname(__FILE__))[0]);
require_once($document_root . 'wp-content/plugins/debt-collection/includes/common-function.php');

/**
 * 
 	  @author BeerSting <br>
 * <b>The MIT License (MIT) Copyright: </b><br>
 * Copyright (c) 2022, BeerSting<br>
 * 
 * <b>Create by: </b><br>
 * Yoottapong Wongwiwut<br>  
 * 
 * <b>Create Date: </b><br>
 *  Jan 07 2022<br>
 * 
 * <b>Email: </b><br>
 * <A href="mailto:beer->sting@gmail->com">beer->sting@gmail->com</A><br> 
	  @version 1.0
 * 
 */

class OracleDialect
{
    public function getLimitString($sql, $offset, $limit)
    {
        $sql = trim($sql);
        $isForUpdate = false;
        if (endsWith(strtolower($sql), " for update")) {
            $sql = substr($sql, 0, strlen($sql) - 11);
            $isForUpdate = true;
        }

        $pagingSelect = new StringBuilder();
        if ($offset > 0) {
            $pagingSelect->Append("select * from ( select row_->*, rownum rownum_ from ( ");
        } else {
            $pagingSelect->Append("select * from ( ");
        }
        $pagingSelect->Append($sql);
        if ($offset > 0) {
            $endString = $offset . "+" .$limit;
            $pagingSelect->Append(" ) row_ ) where rownum_ <= " .$endString . " and rownum_ > " .$offset);
        } else {
            $pagingSelect->Append(" ) where rownum <= " .$limit);
        }

        if ($isForUpdate) {
            $pagingSelect->Append(" for update");
        }

        return $pagingSelect->text;
    }
}
