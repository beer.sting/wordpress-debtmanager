<!-- CSS -->
<style>
    .divContainer {
        /* max-width: 700px;
        width: 100%; */
        background-color: #fff;
        padding: 25px 30px;
        border-radius: 5px;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.15);
    }

    .divContainer .title {
        font-size: 25px;
        font-weight: 500;
        position: relative;
    }

    .divContainer .title::before {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        height: 3px;
        width: 30px;
        border-radius: 5px;
        background: linear-gradient(135deg, #71b7e6, #9b59b6);
    }

    .content form .user-details {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        margin: 20px 0 12px 0;
    }

    form .user-details .input-box {
        margin-bottom: 15px;
        width: calc(100% / 2 - 20px);
    }

    form .input-box span.details {
        display: block;
        font-weight: 500;
        margin-bottom: 5px;
    }

    .user-details .input-box input {
        height: 45px;
        width: 100%;
        outline: none;
        font-size: 16px;
        border-radius: 5px;
        padding-left: 15px;
        border: 1px solid #ccc;
        border-bottom-width: 2px;
        transition: all 0.3s ease;
    }

    .user-details .input-box input:focus,
    .user-details .input-box input:valid {
        border-color: #9b59b6;
    }

    form .gender-details .gender-title {
        font-size: 20px;
        font-weight: 500;
    }

    form .category {
        display: flex;
        /* width: 80%; */
        margin: 14px 0;
        justify-content: space-between;
    }

    form .category label {
        display: flex;
        align-items: center;
        cursor: pointer;
    }

    form .category label .dot {
        height: 18px;
        width: 18px;
        border-radius: 50%;
        margin-right: 10px;
        background: #d9d9d9;
        border: 5px solid transparent;
        transition: all 0.3s ease;
    }

    #sex-1:checked~.category label .one,
    #sex-2:checked~.category label .two,
    #status-1:checked~.category label .one,
    #status-2:checked~.category label .two {
        background: #9b59b6;
        border-color: #d9d9d9;
    }

    form input[type="radio"] {
        display: none;
    }


    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #215387;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        /* display: none; */
        /* padding: 6px 12px; */
        border: 1px solid #ccc;
        border-top: none;
    }
</style>

<!-- HTML -->
<div class="tab">
    <button class="tablinks" onclick="openTab(event, 'customer')">Customer</button>
    <button class="tablinks" onclick="openTab(event, 'address')">Address</button>
    <button class="tablinks" onclick="openTab(event, 'customerInfo')">Customer info</button>
    <button class="tablinks" onclick="openTab(event, 'relationship')">Relationship</button>
</div>

<div id="customer" class="divContainer tabcontent">
    <div class="title">Customer</div>
    <div class="content">
        <form name='formManage' id='formManage'>
            <div class="user-details">
                <div class="input-box">
                    <span class="details">ID card</span>
                    <input name="idcard" type="text" placeholder="" data-val="true" data-val-required='กรุณากรอกข้อมูล ID card.'>
                </div>
                <div class="input-box">
                    <span class="details">Customer code</span>
                    <input name="customerCode" type="text" placeholder="">
                </div>

                <div class="input-box">
                    <span name="title" class="details">Title</span>
                    <select>
                        <option value=""> กรุณาเลือกข้อมูล </option>
                        <option [value]="นาย">นาย</option>
                        <option [value]="นาง">นาง</option>
                        <option [value]="นางสาว">นางสาว</option>
                    </select>
                </div>
                <div class="input-box">
                    <span class="details">First name</span>
                    <input name="fname" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">Last name</span>
                    <input name="lname" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">Birthday</span>
                    <input name="birthday" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">Race (สัญชาติ)</span>
                    <input name="race" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">Religion (ศาสนา)</span>
                    <input name="religion" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">Occupation (อาชีพ)</span>
                    <input name="occupation" type="text" placeholder="">
                </div>

                <div class="gender-details">
                    <input type="radio" name="sex" id="sex-1" value="male">
                    <input type="radio" name="sex" id="sex-2" value="female">
                    <span class="gender-title">Gender (เพศ)</span>
                    <div class="category">
                        <label for="sex-1">
                            <span class="dot one"></span>
                            <span class="gender">Male (ชาย)</span>
                        </label>
                        &nbsp;&nbsp;&nbsp;
                        <label for="sex-2">
                            <span class="dot two"></span>
                            <span class="gender">Female (หญิง)</span>
                        </label>
                    </div>
                </div>

                <div class="gender-details">
                    <input type="radio" name="status" id="status-1" value="single">
                    <input type="radio" name="status" id="status-2" value="marry">
                    <span class="gender-title">Status (สถานะสมรส)</span>
                    <div class="category">
                        <label for="status-1">
                            <span class="dot one"></span>
                            <span class="status">Single (โสด)</span>
                        </label>
                        &nbsp;&nbsp;&nbsp;
                        <label for="status-2">
                            <span class="dot two"></span>
                            <span class="status">Marry (สมรส)</span>
                        </label>
                    </div>
                </div>

                <div class="input-box">
                    <span class="details">Phone (หมายเลขโทรศัพท์)</span>
                    <input name="phone" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">Passport</span>
                    <input name="passport" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">Driver card (ใบขับขี่)</span>
                    <input name="driver_card" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">Email</span>
                    <input name="email" type="text" placeholder="">
                </div>
                <!-- <div class="input-box">
                    <span class="details">Address</span>
                    <textarea name="address"></textarea>
                </div>
                <div class="input-box">
                    <span class="details">ข้อมูลติดต่ออื่นๆ</span>
                    <textarea name="otherContact"></textarea>
                </div> -->
            </div>


            <div id="errorContainer" class="validation-summary-errors" style="display:none;">
                <p>Please correct below errors:</p>
                <ul />
            </div>
            <div>
                <!-- Variable -->
                <!-- <input type="hidden" name="PK" value="" /> -->
                <input type="hidden" name="action" value="Insert" />

                <!-- Button action -->
                <button type="button" name="btnSave" id="btnSave">Save</button>
                <button type="button" name="btnCancel" id="btnCancel">Cancel</button>
                <button type="button" name="btnSearch" id="btnSearch">Search</button>
            </div>

        </form>
    </div>

    <!-- @******* DataGrid *******@ -->
    <hr class="pr-space">
    <?php
    require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGrid.php');
    require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGridController.php');

    $jsGrid = new jsGrid();
    $jsGrid->TABLENAME = "wp_dept_mscustomer";
    $jsGrid->Sql = "SELECT wp_dept_mscustomer.* FROM wp_dept_mscustomer";
    $jsGrid->FormSearchId = "formManage";
    $jsGrid->ButtonSearchId = "btnSearch";
    //$jsGrid->SearchEqual = "phone";
    $jsGrid->SearchLike = "idcard, fname";
    $jsGrid->FormatData = "createDate:=Y-m-d H:i:s->d/m/Y H:i, updateDate:=Y-m-d H:i:s->d/m/Y H:i";
    // $jsGrid->SearchBetween = "[updateDate:startUpdateDate,endUpdateDate]";
    // $jsGrid->SearchBetween = "[updateDate:StartUpdateDate]";
    $jsGrid->width = "100%";
    $jsGrid->height = "300px";
    // $jsGrid->style = "padding: 25px 30px; border-radius: 5px; box-shadow: 0 5px 10px rgba(0, 0, 0, 0.15);";
    $jsGrid->pageSize = 5;
    $jsGrid->fields = "{ name: \"idcard\", title: \"" . "ID Card" . "\", type: \"text\",SkipExport:true, width: 100, align: \"center\" },"
        . "{ name: \"customerCode\", title: \"" . "Customer code" . "\", type: \"text\", width: 100, align: \"center\" },"
        . "{ name: \"title\", title: \"" . "Title" . "\", type: \"text\", width: 100, align: \"left\" },"
        . "{ name: \"fname\", title: \"" . "First name" . "\", type: \"text\", width: 180, align: \"left\" },"
        . "{ name: \"lname\", title: \"" . "Last name" . "\", type: \"text\", width: 180, align: \"left\" },"
        . "{ name: \"phone\", title: \"" . "Phone" . "\", type: \"text\", width: 100, align: \"left\" },"
        . "{ name: \"updatedBy\", title: \"" . "UpdateBy" . "\", type: \"text\", width: 100, align: \"left\" },"
        . "{ name: \"updateDate\", title: \"" . "UpdateDate" . "\", type: \"text\", width: 100, align: \"left\" },"
        . "{ title: \"Edit\", title: \"" . "Edit" . "\", type: \"control\", deleteButton: false, width: 50 },"
        . "{ title: \"Delete\", title: \"" . "Delete" . "\", type: \"control\", editButton: false, width: 50 },";

    $jsGridController = new jsGridController();
    $jsGridController->CreateJsGrid($jsGrid);
    ?>

    <!-- Config -->
    <script type="text/javascript">
        config("wp_dept_mscustomer", "idcard", "<?= $jsGrid->IdJsGrid ?>");
        createEvent("formManage", "database-controller.php", "", "btnSave", "btnCancel");
    </script>

</div>


<div id="address" class="divContainer tabcontent" style="display: none;">
    <div class="title">Address</div>
    <div class="content">
        <form name='formManageAddress' id='formManageAddress'>
            <div class="user-details">

                <div class="input-box">
                    <span class="details">Address (ที่อยู่)</span>
                    <textarea name="address"></textarea>
                </div>

                <div class="input-box">
                    <span class="details">country (ประเทศ)</span>
                    <input name="country" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">districts (ตำบล)</span>
                    <input name="districts" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">amphures (อำเภอ)</span>
                    <input name="amphures" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">province (จังหวัด)</span>
                    <input name="province" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">postal (รหัสไปรษณีย์)</span>
                    <input name="religion" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">contactName (ชื่อคนติดต่อคนที่ 1)</span>
                    <input name="contactName" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">phone (ชื่อคนติดต่อคนที่ 1)</span>
                    <input name="phone" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">contactName (ชื่อคนติดต่อคนที่ 2)</span>
                    <input name="contactName2" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">contactName (ชื่อคนติดต่อคนที่ 2)</span>
                    <input name="phone2" type="text" placeholder="">
                </div>
            </div>


            <div id="errorContainer" class="validation-summary-errors" style="display:none;">
                <p>Please correct below errors:</p>
                <ul />
            </div>
            <div>
                <!-- Variable -->
                <!-- <input type="hidden" name="PK" value="" /> -->
                <input type="hidden" name="action" value="Insert" />

                <!-- Button action -->
                <button type="button" name="btnSaveAddress" id="btnSaveAddress">Save</button>
                <button type="button" name="btnCancelAddress" id="btnCancelAddress">Cancel</button>
                <button type="button" name="btnSearchAddress" id="btnSearchAddress">Search</button>
            </div>
        </form>
    </div>


    <!-- @******* DataGrid *******@ -->
    <hr class="pr-space">
    <?php
    require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGrid.php');
    require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGridController.php');

    $jsGrid = new jsGrid();
    $jsGrid->TABLENAME = "wp_dept_msrelationshipAddress";
    $jsGrid->Sql = "SELECT wp_dept_msrelationship.* FROM wp_dept_msrelationship WHERE relationshipTypeCode='CM'";
    $jsGrid->FormSearchId = "formManageAddress";
    $jsGrid->ButtonSearchId = "btnSearchAddress";
    //$jsGrid->SearchEqual = "phone";
    $jsGrid->SearchLike = "relationshipId";
    $jsGrid->FormatData = "createDate:=Y-m-d H:i:s->d/m/Y H:i, updateDate:=Y-m-d H:i:s->d/m/Y H:i";
    // $jsGrid->SearchBetween = "[updateDate:startUpdateDate,endUpdateDate]";
    // $jsGrid->SearchBetween = "[updateDate:StartUpdateDate]";
    $jsGrid->width = "100%";
    $jsGrid->height = "300px";
    // $jsGrid->style = "padding: 25px 30px; border-radius: 5px; box-shadow: 0 5px 10px rgba(0, 0, 0, 0.15);";
    $jsGrid->pageSize = 5;
    $jsGrid->fields = "{ name: \"idcard\", title: \"" . "ID Card" . "\", type: \"text\",SkipExport:true, width: 100, align: \"center\" },"
        . "{ name: \"fullname\", title: \"" . "fullname" . "\", type: \"text\", width: 180, align: \"left\" },"
        . "{ name: \"address\", title: \"" . "address" . "\", type: \"text\", width: 180, align: \"left\" },"
        . "{ name: \"country\", title: \"" . "country" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"districts\", title: \"" . "districts" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"amphures\", title: \"" . "amphures" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"province\", title: \"" . "province" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"postal\", title: \"" . "postal" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"contactName\", title: \"" . "contactName" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"phone\", title: \"" . "phone" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"contactName2\", title: \"" . "contactName2" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"phone2\", title: \"" . "phone2" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"updatedBy\", title: \"" . "UpdateBy" . "\", type: \"text\", width: 100, align: \"left\" },"
        . "{ name: \"updateDate\", title: \"" . "UpdateDate" . "\", type: \"text\", width: 100, align: \"left\" },"
        . "{ title: \"Edit\", title: \"" . "Edit" . "\", type: \"control\", deleteButton: false, width: 50 },"
        . "{ title: \"Delete\", title: \"" . "Delete" . "\", type: \"control\", editButton: false, width: 50 },";

    $jsGridController = new jsGridController();
    $jsGridController->CreateJsGrid($jsGrid);
    ?>

    <!-- Config -->
    <script type="text/javascript">
        config("wp_dept_msrelationship", "relationshipId", "<?= $jsGrid->IdJsGrid ?>");
        createEvent("formManageAddress", "database-controller.php", "", "btnSaveAddress", "btnCancelAddress");
    </script>

</div>



<div id="relationship" class="divContainer tabcontent" style="display: none;">
    <div class="title">relationship</div>
    <div class="content">
        <form name='formManagerRelationship' id='formManageAddressRelationship'>
            <div class="user-details">

                <div class="input-box">
                    <span class="details">ID card</span>
                    <input name="idcard" type="text" placeholder="" data-val="true" >
                </div>
                <div class="input-box">
                    <span class="details">idcardCustomer (ID card ลูกค้า)</span>
                    <input name="idcardCustomer" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span name="relationshipTypeCode" class="details">ความสัมพันธ์</span>
                    <select>
                        <option value=""> กรุณาเลือกข้อมูล </option>
                        <option [value]="GT">คนค้ำ</option>
                        <option [value]="FT">พ่อ</option>
                        <option [value]="MT">แม่</option>
                        <option [value]="CBR">คนกู้ร่วม</option>
                        <option [value]="OTH">คนติดต่อฉุกเฉิน</option>
                    </select>
                </div>
                <div class="input-box">
                    <span class="details">ชื่อเต็ม</span>
                    <input name="fullname" type="text" placeholder="">
                </div>

                <div class="input-box">
                    <span class="details">Phone (หมายเลขโทรศัพท์)</span>
                    <input name="tel" type="tel" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">Email</span>
                    <input name="email" type="text" placeholder="">
                </div>

                <div class="input-box">
                    <span class="details">Address (ที่อยู่)</span>
                    <textarea name="address"></textarea>
                </div>

                <div class="input-box">
                    <span class="details">country (ประเทศ)</span>
                    <input name="country" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">districts (ตำบล)</span>
                    <input name="districts" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">amphures (อำเภอ)</span>
                    <input name="amphures" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">province (จังหวัด)</span>
                    <input name="province" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">postal (รหัสไปรษณีย์)</span>
                    <input name="religion" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">contactName (ชื่อคนติดต่อคนที่ 1)</span>
                    <input name="contactName" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">phone (ชื่อคนติดต่อคนที่ 1)</span>
                    <input name="phone" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">contactName (ชื่อคนติดต่อคนที่ 2)</span>
                    <input name="contactName2" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">contactName (ชื่อคนติดต่อคนที่ 2)</span>
                    <input name="phone2" type="text" placeholder="">
                </div>
            </div>


            <div id="errorContainer" class="validation-summary-errors" style="display:none;">
                <p>Please correct below errors:</p>
                <ul />
            </div>
            <div>
                <!-- Variable -->
                <!-- <input type="hidden" name="PK" value="" /> -->
                <input type="hidden" name="action" value="Insert" />

                <!-- Button action -->
                <button type="button" name="btnSaveRelationship" id="btnSaveRelationship">Save</button>
                <button type="button" name="btnCancelRelationship" id="btnCancelRelationship">Cancel</button>
                <button type="button" name="btnSearchRelationship" id="btnSearchRelationship">Search</button>
            </div>
        </form>
    </div>


    <!-- @******* DataGrid *******@ -->
    <hr class="pr-space">
    <?php
    require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGrid.php');
    require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGridController.php');

    $jsGrid = new jsGrid();
    $jsGrid->TABLENAME = "wp_dept_msrelationship";
    $jsGrid->Sql = "SELECT wp_dept_msrelationship.* FROM wp_dept_msrelationship WHERE relationshipTypeCode != 'CM'";
    $jsGrid->FormSearchId = "formManageRelationship";
    $jsGrid->ButtonSearchId = "btnSearchRelationship";
    //$jsGrid->SearchEqual = "phone";
    $jsGrid->SearchLike = "relationshipId";
    $jsGrid->FormatData = "createDate:=Y-m-d H:i:s->d/m/Y H:i, updateDate:=Y-m-d H:i:s->d/m/Y H:i";
    // $jsGrid->SearchBetween = "[updateDate:startUpdateDate,endUpdateDate]";
    // $jsGrid->SearchBetween = "[updateDate:StartUpdateDate]";
    $jsGrid->width = "100%";
    $jsGrid->height = "300px";
    // $jsGrid->style = "padding: 25px 30px; border-radius: 5px; box-shadow: 0 5px 10px rgba(0, 0, 0, 0.15);";
    $jsGrid->pageSize = 5;
    $jsGrid->fields = "{ name: \"idcard\", title: \"" . "ID Card" . "\", type: \"text\",SkipExport:true, width: 100, align: \"center\" },"
        . "{ name: \"fullname\", title: \"" . "fullname" . "\", type: \"text\", width: 180, align: \"left\" },"
        . "{ name: \"tel\", title: \"" . "tel" . "\", type: \"text\", width: 180, align: \"left\" },"
        . "{ name: \"email\", title: \"" . "email" . "\", type: \"text\", width: 180, align: \"left\" },"
        . "{ name: \"address\", title: \"" . "address" . "\", type: \"text\", width: 180, align: \"left\" },"
        . "{ name: \"country\", title: \"" . "country" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"districts\", title: \"" . "districts" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"amphures\", title: \"" . "amphures" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"province\", title: \"" . "province" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"postal\", title: \"" . "postal" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"contactName\", title: \"" . "contactName" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"phone\", title: \"" . "phone" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"contactName2\", title: \"" . "contactName2" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"phone2\", title: \"" . "phone2" . "\", type: \"text\", width: 120, align: \"left\" },"
        . "{ name: \"updatedBy\", title: \"" . "UpdateBy" . "\", type: \"text\", width: 100, align: \"left\" },"
        . "{ name: \"updateDate\", title: \"" . "UpdateDate" . "\", type: \"text\", width: 100, align: \"left\" },"
        . "{ title: \"Edit\", title: \"" . "Edit" . "\", type: \"control\", deleteButton: false, width: 50 },"
        . "{ title: \"Delete\", title: \"" . "Delete" . "\", type: \"control\", editButton: false, width: 50 },";

    $jsGridController = new jsGridController();
    $jsGridController->CreateJsGrid($jsGrid);
    ?>

    <!-- Config -->
    <script type="text/javascript">
        config("wp_dept_msrelationship", "relationshipId", "<?= $jsGrid->IdJsGrid ?>");
        createEvent("formManageRelationship", "database-controller.php", "", "btnSaveRelationship", "btnCancelRelationship");
    </script>

</div>


<div id="customerInfo" class="divContainer tabcontent" style="display: none;">
    <div class="title">Customer Info</div>
    <div class="content">
        <form name='formManageCustomerinfo' id='formManageCustomerinfo'>
            <div class="user-details">
                <!-- <div class="input-box">
                    <span class="details">ID card</span>
                    <input name="idcard" type="text" placeholder="" data-val="true" data-val-required='กรุณากรอกข้อมูล ID card.'>
                </div>
                <div class="input-box">
                    <span class="details">First name</span>
                    <input name="fname" type="text" placeholder="">
                </div> -->

                <div class="input-box">
                    <span class="details">เลือกชนิดข้อมูล</span>
                    <select name="fieldType">
                        <option value=""> กรุณาเลือกข้อมูล </option>
                        <option [value]="Phone">เบอร์โทร</option>
                        <option [value]="Mobile">เบอร์มือถือ</option>
                    </select>
                </div>
                <div class="input-box">
                    <span class="details">ข้อมูล</span>
                    <input name="value" type="text" placeholder="">
                </div>

                <div id="errorContainer" class="validation-summary-errors" style="display:none;">
                    <p>Please correct below errors:</p>
                    <ul />
                </div>
                <div>
                    <!-- Variable -->
                    <!-- <input type="hidden" name="PK" value="" /> -->
                    <input type="hidden" name="action" value="Insert" />

                    <!-- Button action -->
                    <button type="button" name="btnSaveCustomerinfo" id="btnSaveCustomerinfo">Save</button>
                    <button type="button" name="btnCancelCustomerinfo" id="btnCancelCustomerinfo">Cancel</button>
                    <button type="button" name="btnSearchCustomerinfo" id="btnSearchCustomerinfo">Search</button>
                </div>

        </form>
    </div>

    <!-- @******* DataGrid *******@ -->
    <hr class="pr-space">
    <?php
    require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGrid.php');
    require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGridController.php');

    $jsGrid = new jsGrid();
    $jsGrid->TABLENAME = "wp_dept_mscustomerinfo";
    $jsGrid->Sql = "SELECT wp_dept_mscustomerinfo.* FROM wp_dept_mscustomerinfo";
    $jsGrid->FormSearchId = "formManageCustomerinfo";
    $jsGrid->ButtonSearchId = "btnSearchCustomerinfo";
    //$jsGrid->SearchEqual = "phone";
    $jsGrid->SearchLike = "idcard";
    $jsGrid->FormatData = "createDate:=Y-m-d H:i:s->d/m/Y H:i, updateDate:=Y-m-d H:i:s->d/m/Y H:i";
    // $jsGrid->SearchBetween = "[updateDate:startUpdateDate,endUpdateDate]";
    // $jsGrid->SearchBetween = "[updateDate:StartUpdateDate]";
    $jsGrid->width = "100%";
    $jsGrid->height = "300px";
    // $jsGrid->style = "padding: 25px 30px; border-radius: 5px; box-shadow: 0 5px 10px rgba(0, 0, 0, 0.15);";
    $jsGrid->pageSize = 5;
    $jsGrid->fields = "{ name: \"idcard\", title: \"" . "ID Card" . "\", type: \"text\",SkipExport:true, width: 100, align: \"center\" },"
        . "{ name: \"fieldType\", title: \"" . "Type" . "\", type: \"text\", width: 100, align: \"left\" },"
        . "{ name: \"value\", title: \"" . "value" . "\", type: \"text\", width: 180, align: \"left\" },"
        . "{ name: \"updatedBy\", title: \"" . "UpdateBy" . "\", type: \"text\", width: 100, align: \"left\" },"
        . "{ name: \"updateDate\", title: \"" . "UpdateDate" . "\", type: \"text\", width: 100, align: \"left\" },"
        . "{ title: \"Edit\", title: \"" . "Edit" . "\", type: \"control\", deleteButton: false, width: 50 },"
        . "{ title: \"Delete\", title: \"" . "Delete" . "\", type: \"control\", editButton: false, width: 50 },";

    $jsGridController = new jsGridController();
    $jsGridController->CreateJsGrid($jsGrid);
    ?>

    <!-- Config -->
    <script type="text/javascript">
        config("wp_dept_mscustomerinfo", "customerInfoId", "<?= $jsGrid->IdJsGrid ?>");
        createEvent("formManageCustomerinfo", "database-controller.php", "", "btnSaveCustomerinfo", "btnCancelCustomerinfo");
    </script>

</div>


<!-- openTab -->
<script type="text/javascript">
    function openTab(evt, cityName) {
        // Declare all variables
        var i, tabcontent, tablinks;

        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        // Show the current tab, and add an "active" class to the button that opened the tab
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>