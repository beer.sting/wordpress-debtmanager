<!-- CSS -->
<style>
    .divContainer {
        /* max-width: 700px;
        width: 100%; */
        background-color: #fff;
        padding: 25px 30px;
        border-radius: 5px;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.15);
    }

    .divContainer .title {
        font-size: 25px;
        font-weight: 500;
        position: relative;
    }

    .divContainer .title::before {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        height: 3px;
        width: 30px;
        border-radius: 5px;
        background: linear-gradient(135deg, #71b7e6, #9b59b6);
    }

    .content form .user-details {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        margin: 20px 0 12px 0;
    }

    form .user-details .input-box {
        margin-bottom: 15px;
        width: calc(100% / 2 - 20px);
    }

    form .input-box span.details {
        display: block;
        font-weight: 500;
        margin-bottom: 5px;
    }

    .user-details .input-box input {
        height: 45px;
        width: 100%;
        outline: none;
        font-size: 16px;
        border-radius: 5px;
        padding-left: 15px;
        border: 1px solid #ccc;
        border-bottom-width: 2px;
        transition: all 0.3s ease;
    }

    .user-details .input-box input:focus,
    .user-details .input-box input:valid {
        border-color: #9b59b6;
    }

    form .gender-details .gender-title {
        font-size: 20px;
        font-weight: 500;
    }

    form .category {
        display: flex;
        width: 80%;
        margin: 14px 0;
        justify-content: space-between;
    }

    form .category label {
        display: flex;
        align-items: center;
        cursor: pointer;
    }

    form .category label .dot {
        height: 18px;
        width: 18px;
        border-radius: 50%;
        margin-right: 10px;
        background: #d9d9d9;
        border: 5px solid transparent;
        transition: all 0.3s ease;
    }

    #dot-1:checked~.category label .one,
    #dot-2:checked~.category label .two,
    #dot-3:checked~.category label .three {
        background: #9b59b6;
        border-color: #d9d9d9;
    }

    form input[type="radio"] {
        display: none;
    }
</style>


<!-- HTML -->
<div class="divContainer">
    <div class="title">Username</div>
    <div class="content">
        <form name='formManage' id='formManage'>
            <div class="user-details">
                <div class="input-box">
                    <span class="details">username</span>
                    <input name="username" type="text" placeholder="" data-val="true" data-val-required='กรุณากรอกข้อมูล ID card.'>
                </div>
                <div class="input-box">
                    <span class="details">name</span>
                    <input name="name" type="text" placeholder="">
                </div>
                <div class="input-box">
                    <span class="details">password</span>
                    <input name="password" type="text" placeholder="">
                </div>
            </div>

            <div id="errorContainer" class="validation-summary-errors" style="display:none;">
                <p>Please correct below errors:</p>
                <ul />
            </div>
            <div>
                <!-- Variable -->
                <!-- <input type="hidden" name="PK" value="" /> -->
                <input type="hidden" name="action" value="Insert" />

                <!-- Button action -->
                <button type="button" name="btnSave" id="btnSave">Save</button>
                <button type="button" name="btnCancel" id="btnCancel">Cancel</button>
                <button type="button" name="btnSearch" id="btnSearch">Search</button>
            </div>

        </form>
    </div>
</div>

<hr class="pr-space">
<?php
require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGrid.php');
require_once($document_root . 'wp-content/plugins/debt-collection/includes/jsGrid/jsGridController.php');

$jsGrid = new jsGrid();
$jsGrid->TABLENAME = "wp_dept_syuser";
$jsGrid->Sql = "SELECT * FROM wp_dept_syuser";
$jsGrid->FormSearchId = "formManage";
$jsGrid->ButtonSearchId = "btnSearch";
$jsGrid->SearchLike = "username, name";
$jsGrid->FormatData = "updateDate:=Y-m-d H:i:s->d/m/Y H:i";
$jsGrid->width = "100%";
$jsGrid->height = "200px";
// $jsGrid->style = "padding: 25px 30px; border-radius: 5px; box-shadow: 0 5px 10px rgba(0, 0, 0, 0.15);";
$jsGrid->pageSize = 2;
$jsGrid->fields = "{ name: \"username\", title: \"" . "username" . "\", type: \"text\",SkipExport:true, width: 100, align: \"center\" },"
    . "{ name: \"name\", title: \"" . "name" . "\", type: \"text\", width: 100, align: \"center\" },"
    . "{ name: \"fname\", title: \"" . "fname" . "\", type: \"text\", width: 180, align: \"left\" },"
    . "{ name: \"password\", title: \"" . "password" . "\", type: \"text\", width: 180, align: \"left\" },"
    . "{ name: \"updatedBy\", title: \"" . "UpdateBy" . "\", type: \"text\", width: 100, align: \"left\" },"
    . "{ name: \"updateDate\", title: \"" . "UpdateDate" . "\", type: \"text\", width: 100, align: \"left\" },"
    . "{ title: \"Edit\", title: \"" . "Edit" . "\", type: \"control\", deleteButton: false, width: 50 },"
    . "{ title: \"Delete\", title: \"" . "Delete" . "\", type: \"control\", editButton: false, width: 50 },";

$jsGridController = new jsGridController();
$jsGridController->CreateJsGrid($jsGrid);
?>

<!-- Config -->
<script type="text/javascript">
    config("wp_dept_syuser", "username", "<?= $jsGrid->IdJsGrid ?>");
    createEvent("formManage", "database-controller.php", "", "btnSave", "btnCancel");
</script>