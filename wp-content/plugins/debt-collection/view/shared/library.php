<!-- <link href="~/Resources/Template/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet"> -->

<!-- Jquery -->
<script src="../wp-content/plugins/debt-collection/assets/js/jquery/jquery-2.2.0.js"></script>
<script src="../wp-content/plugins/debt-collection/assets/js/jquery/jquery.serializeObject.js"></script>

<!-- Jquery Validation -->
<link href="../wp-content/plugins/debt-collection/assets/js/jquery-validate/validate.css" rel="stylesheet">
<script src="../wp-content/plugins/debt-collection/assets/js/jquery-validate/jquery.validate.min.js"></script>
<script src="../wp-content/plugins/debt-collection/assets/js/jquery-validate/jquery.unobtrusive-ajax.min.js"></script>
<script src="../wp-content/plugins/debt-collection/assets/js/jquery-validate/jquery.validate.unobtrusive.min.js"></script>

<!-- Jquery Confirm -->
<script src="../wp-content/plugins/debt-collection/assets/js/jquery-confirm/jquery-confirm.js"></script>
<link href="../wp-content/plugins/debt-collection/assets/js/jquery-confirm/jquery-confirm.css" rel="stylesheet">

<!-- Datepicker -->
<!-- <script src="../wp-content/plugins/debt-collection/assets/js/datepicker/bootstrap-datepicker.js"></script>
<script src="../wp-content/plugins/debt-collection/assets/js/datepicker/bootstrap-datepicker-thai.js"></script>
<script src="../wp-content/plugins/debt-collection/assets/js/datepicker/locales//bootstrap-datepicker.th.js"></script>
<link href="../wp-content/plugins/debt-collection/assets/js/datepicker/datepicker.css" rel="stylesheet">
<link href="../wp-content/plugins/debt-collection/assets/js/datepicker/bootstrap-datetimepicker.css" rel="stylesheet"> -->

<!-- JsGrid -->
<script src="../wp-content/plugins/debt-collection/assets/js/Jsgrid/dist/jsgrid.js"></script>
<link href="../wp-content/plugins/debt-collection/assets/js/Jsgrid/dist/jsgrid.css" rel="stylesheet">
<link href="../wp-content/plugins/debt-collection/assets/js/Jsgrid/css/theme.css" rel="stylesheet">

<!-- Main Function -->
<script src="../wp-content/plugins/debt-collection/assets/js/BeerSting.js"></script>
<script src="../wp-content/plugins/debt-collection/assets/js/action.js"></script>

<!-- Loading -->
<link href="../wp-content/plugins/debt-collection/assets/js/loading/overlay.css" rel="stylesheet">

<!-- Toastr -->
<link href="../wp-content/plugins/debt-collection/assets/js/toastr/toastr.min.css" rel="stylesheet">
<script src="../wp-content/plugins/debt-collection/assets/js/toastr/toastr.min.js"></script>

<!-- Layout -->
<div id="overlay" style="display: none;">
    <span id="spnTextOverlay">
        Loading... <img alt="" src="../wp-content/plugins/debt-collection/assets/js/loading/img/loader.gif" style="width:32px;height:32px;margin-top: -60px;
    margin-left: 120px;">
    </span>
</div>

<script type="text/javascript">
    //*** Defaults Validator
    if ($.validator) {
        $.validator.setDefaults({
            ignore: [],
            // any other default options and/or rules
        });
    }

    //*** Defaults Toastr
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>