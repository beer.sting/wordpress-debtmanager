<?php
$document_root = str_replace("\\", "/", explode("wp-content", dirname(__FILE__))[0]);
require_once($document_root . 'wp-content/plugins/debt-collection/includes/config.php');
require_once($document_root . 'wp-content/plugins/debt-collection/model/AjaxResult.php');
require_once($document_root . 'wp-config.php');
//error_reporting(E_ERROR | E_PARSE);
 
global $wpdb;
date_default_timezone_set('Asia/Bangkok');

function execute()
{
	global $keySystem, $ajaxResult, $wpdb;
	try {
		$ajaxResult = new AjaxResult();
		$func = $_POST['func'];
		$action = $_POST['action'];
		$tablename = $wpdb->prefix . explode("_", $_POST['table'])[1] . "_" . explode("_", $_POST['table'])[2];
		$data = $_POST['data'];

		$current_user = wp_get_current_user();
		$user_login = $current_user->user_login;
		$dateTime = date("Y-m-d H:i:s");

		//** Customize data
		$data = new ArrayObject;
		foreach ($_POST as $key => $value) {
			if (is_array($key)) {
				foreach ($key as $key2 => $value2) {
					if(!array_search($key2, $keySystem) && is_bool(array_search($key2, $keySystem))){
						if($value2) $data[$key][$key2] = $value2;
					} 
				}
			} else {
				if(!array_search($key, $keySystem) && is_bool(array_search($key, $keySystem))){
					if($value) $data[$key] = $value;
				} 
			}
		}
		$data = (array) $data;

		//** Get PK
		$pkWhere = new ArrayObject;
		$PK = ($_POST["PK"])? $_POST["PK"]:null;
		$list_pk = explode(',', $PK);
		for ($i = 0; $i < count($list_pk); $i++) {
			$pkWhere[trim($list_pk[$i])] = $_POST[trim($list_pk[$i])];
		}
		$pkWhere = (array) $pkWhere;

		// var_dump($_POST);
		// var_dump($data);
		// var_dump($pkWhere);
		
		//** Check Function
		switch ($func) {
			case "Database":
				switch ($action) {
					case "Insert":
						$data['createdBy'] = $user_login;
						$data['createDate'] = $dateTime;
						$data['updatedBy'] = $user_login;
						$data['updateDate'] = $dateTime;
						$wpdb->insert($tablename, $data);
						break;
					case "Update":
						$data['updatedBy'] = $user_login;
						$data['updateDate'] = $dateTime;
						$wpdb->update($tablename, $data, $pkWhere);
						break;
					case "Delete":
						$wpdb->delete($tablename, $pkWhere);
						break;
				}
				break;
		}
		//$ajaxResult->value = $data;
		$ajaxResult->text = "Save success.";
		$ajaxResult->code = "200";
		$ajaxResult->status = "Success";
		echo json_encode($ajaxResult);
	} catch (Exception $e) {
		$ajaxResult->code = "500";
		$ajaxResult->status = "Error";
		$ajaxResult->text = "Error: " . $e->getMessage();
		echo json_encode($ajaxResult);
	}
}

execute();