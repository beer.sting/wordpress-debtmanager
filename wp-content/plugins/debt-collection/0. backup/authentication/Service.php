<?php
session_start();
$document_root = str_replace("\\","/",explode("wp-content", dirname(__FILE__))[0]);
require_once($document_root .'wp-content/plugins/hm-addon/includes/classes/AjaxResult.php');
require_once($document_root .'wp-config.php');
require_once ($document_root .'wp-includes/class-phpass.php');
 
global $wpdb;

//** function
function error($text = null)
{ 
	if($text == null){
		$text = "Error! กรุณาติดต่อผู้ดูแลระบบ.  <a href=\"../../../../../\">Click to Home page</a>";
	}else{
		$text = $text ." <a href=\"../../../../../\">Click to Home page</a>";
	}
	echo $text;
	exit;
}

//** Main Code */
try {
	$ajaxResult = new AjaxResult();
	$func = $_GET['func'];
	$uuid = $_GET['uuid'];
	$password = "_secure";
	$token = $_GET['token'];
	$token_key = explode("/", $_GET['token'])[0];
	$api = explode("/", $_GET['token'])[1];
	$module = $_GET["module"];
	$dateTime = date("Y-m-d H:i:s");
	$apiData;
	$apiObject;
	
	if($uuid != null){
		$_SESSION["UUID"] = $uuid;
		$file_pointer = $uuid . ".txt";
        if (file_exists($file_pointer)) {
			$myfile = fopen($file_pointer, "r") or die("Unable to open file!");
			$token =  fread($myfile,filesize($file_pointer));
			fclose($myfile);
			$token_key = explode("/", $token)[0];
			$api = explode("/", $token)[1];
			$module = "cmu";
			$func = "Login";
			//echo $token;
			//exit;
			
        }else {
            header("Location: ".get_home_url());
			exit;
        }
		
	}

	if($func != "Logout"){
		//** Check USER
		$data_token = $wpdb->get_row("SELECT * FROM " .$wpdb->prefix ."hm_users_authentication_token WHERE module = '" .$module ."' AND token = '" .$token ."' AND active = 'Y'");
		if ($data_token == null){	
			$url = "https://isuandok.med.cmu.ac.th/itcc/CMUOAuth/basicinfo.php?token=" .$token_key ."&api=" .$api;
			$apiData = CallAPI("GET", $url, null);
			if($apiData == null || $apiData == ""){
				error();
			}else{
				$apiObject = json_decode($apiData);			
				if($apiObject -> Message != null && $apiObject -> Message != "Unauthorized"){
					error("Error! " .$apiObject -> Message);
				}
				if($apiObject == null || $apiObject -> Message == "Unauthorized"){
					$func = "RedirectLogin";
				}else{
					if ($api == "4"){	
						$apiNewObject = (object) 
							[
							 'PrenameTha' => $apiObject->prename_TH
							,'PrenameEng' => $apiObject->prename_EN
							,'NameTha' => $apiObject->first_name_TH
							,'NameEng' => $apiObject->first_name_EN
							,'MiddleNameTha' => null
							,'MiddleNameEng' => null
							,'SurNameTha' => $apiObject->last_name_TH
							,'SurNameEng' => $apiObject->last_name_EN
							,'WorkStatusNameTha' => $apiObject->status_name
							,'PositionNameTha' => "นักศึกษา" .$apiObject->department_name_TH
							,'OrganizationID1' => $apiObject->faculty_code
							,'OrganizationName1' => $apiObject->faculty_name_TH
							,'OrganizationID2' => $apiObject->department_name_EN
							,'OrganizationName2' => $apiObject->department_name_TH
							,'OrganizationID3' => null
							,'OrganizationName3' => null
							,'OrganizationID4' => null
							,'OrganizationName4' => null
							,'Email' => $apiObject->cmuitaccount						
							];
						$apiObject = $apiNewObject;
						$find = "แพทย";
						$pos = strrpos($apiObject->OrganizationName2 , $find);
						if ($pos === false) {
							error("Error!, User ไม่ใช่นักศึกษาแพทยศาสตร์.");
							exit;
						}
					}
					$func = "RegisterUser";
				}	
			}
		}else if($data_token != null && $func == null){
			$func = "Login";
		}
	}
	
	//** Check Function
	switch ($func) {
		case "RegisterUser":	
			//* Create User HM Addon
			$data_user = $wpdb->get_row("SELECT * FROM " .$wpdb->prefix ."hm_users_authentication WHERE module = '" .$module ."' AND user_login = '" .$apiObject->Email ."' AND active = 'Y'");
			if($data_user == null){
				$tablename=$wpdb->prefix.'hm_users_authentication';
				$data=array(
					 'module' => $module, 
					 'user_login' => $apiObject->Email,
					 'preNameThai' => $apiObject->PrenameTha,
					 'preNameEng' => $apiObject->PrenameEng,
					 'nameThai' => $apiObject->NameTha,
					 'nameEng' => $apiObject->NameEng,
					 'middleNameThai' => $apiObject->MiddleNameTha,
					 'middleNameEng' => $apiObject->MiddleNameEng,
					 'surNameThai' => $apiObject->SurNameTha,
					 'surNameEng' => $apiObject->SurNameEng,
					 'workStatusNameThai' => $apiObject->WorkStatusNameTha,
					 'positionNameThai' => $apiObject->PositionNameTha,
					 'organizationID1' => $apiObject->OrganizationID1,
					 'organizationName1' => $apiObject->OrganizationName1,
					 'organizationID2' => $apiObject->OrganizationID2,
					 'organizationName2' => $apiObject->OrganizationName2,
					 'organizationID3' => $apiObject->OrganizationID3,
					 'organizationName3' => $apiObject->OrganizationName3,
					 'organizationID4' => $apiObject->OrganizationID4,
					 'organizationName4' => $apiObject->OrganizationName4,
					 'email' => $apiObject->Email,
					 'active' => "Y",
					 'create_by' => "System",
 					 'created_date' => $dateTime
				);
				$ajaxResult -> value = $wpdb->insert( 
					$tablename, 
					$data, 
					array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s') 
				);
			
				//* Create User WP*
				$new_password = $apiObject->Email .$password;
				$user_id = wp_create_user($apiObject->Email, $new_password, $apiObject->Email);
				if (is_wp_error( $user_id ) ) {
					echo $user->get_error_message();
					exit;
				}
				//* Update Display name
				$display_name = $apiObject->PrenameTha . " " .$apiObject->NameTha ." " .$apiObject->SurNameTha;
				$user_id = wp_update_user( array( 'ID' => $user_id, 'display_name' => $display_name ) );
				if (is_wp_error( $user_id ) ) {
					echo $user->get_error_message();
					exit;
				}

			}
			
			//* Create Token
			$tablename=$wpdb->prefix.'hm_users_authentication_token';
			$data=array(
					'module' => $module, 
					'user_login' => $apiObject->Email,
					'token' => $token,
					'data' => $apiData,
					'active' => 'Y',
					'create_by' => "system",
					'created_date' => $dateTime
			);
			$ajaxResult -> value = $wpdb->insert( 
					$tablename, 
					$data, 
					array('%s','%s','%s','%s','%s','%s','%s') 
			);

			if ($data_user == null && $api == "3"){	
				goto RedirectRole;
			}else if ($api == "3" || $api == "4"){	
				goto Login;
			}
			break;
		case "RegisterRole":

			$uid = $_GET['uid'];
			$roleid = $_GET['roleid'];
			$firstname = $_GET['fname'];
			$lastname = $_GET['sname'];
			$email = $_GET['email'];

			$data_user = $wpdb->get_row("SELECT * FROM " .$wpdb->prefix ."js_ticket_staff WHERE uid = " . $uid);
			if($data_user == null){
				$tablename=$wpdb->prefix.'js_ticket_staff';
				$data=array(
					'uid' => $uid, 
					'roleid' => $roleid,
					'firstname' => $firstname,
					'lastname' => $lastname,
					'email' => $email,
					'appendsignature' => '1',
					'status' => '1',
					'created' => $dateTime
				);
				$ajaxResult -> value = $wpdb->insert( 
					$tablename, 
					$data, 
					array('%s','%s','%s','%s','%s','%s','%s','%s') 
				);
				$staff = $wpdb->get_row("SELECT * FROM " .$wpdb->prefix ."js_ticket_staff WHERE uid = " . $uid);
				if($staff != null){
					//delete any other acl user permissions which is not the current role id
					$query = "DELETE FROM `" . $wpdb->prefix . "js_ticket_acl_user_permissions` WHERE uid = " . $staff->uid;
					$wpdb->query($query);
					$query = "INSERT INTO `" . $wpdb->prefix . "js_ticket_acl_user_permissions` (uid, roleid, staffid, permissionid, `grant`, status)
							SELECT '" . $staff->uid . "' AS uid,'" . $staff->roleid . "' AS roleid,'" . $staff->id . "' AS staffid, permissionid, `grant`, status FROM `" . $wpdb->prefix . "js_ticket_acl_role_permissions` WHERE roleid = " . $staff->roleid;
					$wpdb->query($query);
					//delete any other acl user permissions which is not the current role id
					$query = "DELETE FROM `" . $wpdb->prefix . "js_ticket_acl_user_access_departments` WHERE uid = " . $staff->uid;
					$wpdb->query($query);
					$query = "INSERT INTO `" . $wpdb->prefix . "js_ticket_acl_user_access_departments` (uid, roleid, staffid, departmentid, status)
							SELECT '" . $staff->uid . "' AS uid,'" . $staff->roleid . "' AS roleid,'" . $staff->id . "' AS staffid, departmentid, status FROM `" . $wpdb->prefix . "js_ticket_acl_role_access_departments` WHERE roleid = " . $staff->roleid;
					$wpdb->query($query);
				}
			}
			goto Login;
			break;
		case "Login":
			goto Login;
			break;
		case "RedirectLogin":
			goto RedirectLogin;
			break;
		case "Logout":
		
			if ($_SESSION["UUID"] != null){
				unlink($_SESSION["UUID"] . ".txt");
			}
			$current_user = wp_get_current_user();
			$email =  $current_user->user_login;
			$query ="UPDATE ". $wpdb->prefix ."hm_users_authentication_token SET active='N' WHERE user_login = '". $current_user->user_login . "'";
			$wpdb->query($query);
			wp_logout();
			goto RedirectIndex;
			
			break;
		case "Redirect":
			Login: {
				if ($data_token == null){	
					$data_token = $wpdb->get_row("SELECT * FROM " .$wpdb->prefix ."hm_users_authentication_token WHERE module = '" .$module ."' AND token = '" .$token ."' AND active = 'Y'");
				}
				$wp_user = $wpdb->get_row("SELECT * FROM " .$wpdb->prefix ."users WHERE user_login = '" .$data_token ->user_login ."'");
				if($wp_user != null){
					$password = $wp_user->user_login .$password;
					$password_hashed = $wp_user->user_pass;
					$wp_hasher = new PasswordHash(8, TRUE);
					if($wp_hasher->CheckPassword($password, $password_hashed)) {
						$creds = array(
							'user_login'    => $wp_user->user_login,
							'user_password' => $password,
							'remember'      => true
						);		 
						
						$user = wp_signon( $creds, false );			 
						if (is_wp_error( $user ) ) {
							echo $user->get_error_message();
							exit;
						}
						if($_SESSION["UUID"] != null){
							$myfile = fopen($_SESSION["UUID"] . ".txt", "w") or die("Unable to open file!");
							$txt = $token;
							fwrite($myfile, $txt);
							fclose($myfile);
						}

						goto RedirectIndex;
					} else {
						error("Error!, Wrong Password.");
						exit;
					}
				}else{
					error();
				}
				

				
				break;
			}
			RedirectLogin: {
				header("Location: https://isuandok.med.cmu.ac.th/itcc/CMUOAuth/login_page.php?app=voc&api=" .$api);
				break;
			}
			RedirectIndex: {
				header("Location: ".get_home_url());
				break;
			}
			RedirectRole: {
				header("Location: RegisterRole.php?token=".$token);
				break;
			}
			break;
	}

} catch (Exception $e) {
	$ajaxResult -> code = "500";
	$ajaxResult -> status = "Error";
	$ajaxResult -> text = "(OTP) Error: " .$e->getMessage();
}
		
//*** Return Data */
echo json_encode($ajaxResult);
?>

