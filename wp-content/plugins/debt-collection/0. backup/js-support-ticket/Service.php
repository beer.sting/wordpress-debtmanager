<?php
$document_root = str_replace("\\","/",explode("wp-content", dirname(__FILE__))[0]);
require_once($document_root .'wp-content/plugins/hm-addon/includes/classes/AjaxResult.php');
require_once($document_root .'wp-config.php');

date_default_timezone_set("Thailand/Bangkok");
$current_user = wp_get_current_user();
$dateTime = date("Y-m-d H:i:s");


function SendEmail($to,$subject,$txt,$headers) {
		global $ajaxResult,$wpdb,$current_user,$dateTime,$log,$result,$status;
		$status = "Y";
		try {
			$result = mail($to,$subject,$txt,$headers);
		} catch (Exception $e) {
			$log = $e->getMessage();
			$status = "N";
		}
		
		$parameter = [
			'to' => $to ,
			'subject' => $subject,
			'txt' => $txt,
			'headers' => $headers			
		];
		
		$tablename=$wpdb->prefix.'hm_log';
		$data=array(
			'module' => "js-support-ticket", 
			'logName' => "Email",
			'action' => "Send",
			'parameter' => json_encode($parameter, JSON_UNESCAPED_UNICODE),
			'log' => $log,
			'result' => $result,
			'status' => (($result == 1)?"Y":"N"),
			'remark' => null,
			'createdBy' => "System",
			'createDate' => $dateTime
		);
		$wpdb->insert( $tablename, $data
			,array( 
				'%s',
				'%s', 
				'%s',
				'%s', 
				'%s', 				
				'%s',
				'%s',
				'%s', 
				'%s', 				 				
				'%s' 
			) 
		);	
}

	
function SendSMS($tel,$text) {
		global $ajaxResult,$wpdb,$current_user,$dateTime,$log,$result,$status;
		$status = "Y";
		$url = "https://w3.med.cmu.ac.th/api/v1/sms";
		$token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NjY5MDEzNTIsImp0aSI6Ii01ZDY1MDQ2ODRhZDA2IiwiaXNzIjoidzMubWVkLmNtdS5hYy50aCIsIm5iZiI6MTU2NjkwMTM1MiwiZXhwIjoxNjYxNTA5MzUyLCJkYXRhIjp7ImFwcCI6InZvYyIsImdyb3VwX2lkIjo5OX19.lktFgG84spQ8myl10W0uId4dMHVvwWPJOKXioKrIj78";
		$app_name = "voc";
		$app_id = "d894c50df83df8ed3d3066912ae201caad039a5ada38ec27efae95dc3843caab9ba7fe34a2f0b460ad5be985495daf4557dde04266412b8a004cd6a3ca26ae1";
        	$authorization = "Authorization: ".$token; 
	   
		$parameter = [
			'tel' => $tel ,
			'text' => $text,
			'app_id' => $app_id,
			'app_name' => $app_name			
		];
		try {
			$result = CallAPI("POST", $url, $parameter, $authorization, "json");	
		} catch (Exception $e) {
			$log = $e->getMessage();
			$status = "N0";
		}
		//echo "result = " .$result;
		//echo 'Current PHP Info: ' . phpinfo();
		//exit;
		if($status != "N"){
			if(empty($result) == false){
				$resultObject = json_decode($result);
				if($resultObject != null && $resultObject->Status == "1"){
					$status = "Y";
				}else{
					$status = "N";
				}
			}else{
					$status = "N";
			}
		}
		
		$tablename=$wpdb->prefix.'hm_log';
		$data=array(
			'module' => "js-support-ticket", 
			'logName' => "SMS",
			'action' => "Send",
			'parameter' => json_encode($parameter, JSON_UNESCAPED_UNICODE),
			'log' => $log,
			'result' => $result,
			'status' => $status,
			'remark' => null,
			'createdBy' => "System",
			'createDate' => $dateTime
		);
		$wpdb->insert( $tablename, $data
			,array( 
				'%s',
				'%s', 
				'%s',
				'%s', 
				'%s', 				
				'%s',
				'%s',
				'%s', 
				'%s', 				 				
				'%s' 
			) 
		);	
}


function CreateOTP($ajaxResult) {
	global $wpdb,$current_user,$dateTime;
	$SecretCode = rand(1000,9999);
	$active = "N";
	$date = date("Y-m-d");
	
	try {
		$tablename=$wpdb->prefix.'hm_js_ticket_secret';
		$data=array(
			'secretCode' => $SecretCode, 
			'email' => $_POST['email'],
			'phone' => $_POST['phone'],
			'ip' => get_client_ip(),
			'user_login' => $current_user->user_login,
			'active' => 'N',
			'createdBy' => "System",
			'createDate' => $dateTime
		);
		$ajaxResult -> value = $wpdb->insert( $tablename, $data
			,array( 
				'%s',
				'%s', 
				'%s',
				'%s', 
				'%s', 				
				'%s', 
				'%s', 
				'%s' 
			) 
		);	
		$secret_data = $wpdb->get_row("SELECT * FROM $tablename WHERE secretCode = $SecretCode AND active = 'N' AND DATE_FORMAT(createDate, '%Y-%m-%d')  = '$date' ");
		$ajaxResult -> id = $secret_data->id;
		$ajaxResult -> value = $SecretCode;
		$ajaxResult -> code = "200";
		$ajaxResult -> status = "Success";
		$ajaxResult -> text = "ระบบทำการส่งรหัส OTP กรุณาตรวจสอบ Email.";
	} catch (Exception $e) {
		$ajaxResult -> code = "500";
		$ajaxResult -> status = "Error";
		$ajaxResult -> text = "(CreateOTP) Error: " .$e->getMessage();
	}
	return $ajaxResult;
}

function SendOTP($ajaxResult) {
	global $wpdb,$current_user,$dateTime;
	try {
		$tablename=$wpdb->prefix.'hm_js_ticket_secret';
		$data_code = $_POST['secureCode'];
		if($data_code != ''){
			$secretCode = substr($data_code,0,4);
			$SecretId = substr($data_code,4);
			$dateTime = date("Y-m-d");		
			$mylink = $wpdb->get_row("SELECT * FROM $tablename WHERE id = " .$SecretId ." AND secretCode = '" .$secretCode ."' AND active = 'N' AND DATE_FORMAT(createDate, '%Y-%m-%d')  = '$dateTime' ", ARRAY_N );
			if ($mylink != null){
				$wpdb->update( 
					$tablename , 
					array( 'active' => 'Y'	), 
					array( 'id' => $SecretId ), 
					array( '%s'	), 
					array( '%d' ) 
				);
				$ajaxResult -> code = "200";
				$ajaxResult -> status = "Success";
				$ajaxResult -> text = "Save Success.";

			}else {
				$ajaxResult -> code = "500";
				$ajaxResult -> status = "Error";
				$ajaxResult -> text = "OTP Incorrect";
			}
		}else{
			$ajaxResult -> code = "500";
			$ajaxResult -> status = "Error";
			$ajaxResult -> text = "OTP Incorrect";
		}
	} catch (Exception $e) {
		$ajaxResult -> code = "500";
		$ajaxResult -> status = "Error";
		$ajaxResult -> text = "(OTP) Error: " .$e->getMessage();
	}
	return $ajaxResult;

}

//** Main Code */
$ajaxResult = new AjaxResult();
$func = $_POST["func"];
$ajaxResult -> comment = $func;

if($func == "CreateOTP"){
	$ajaxResult = CreateOTP($ajaxResult);
	$SecretId = $ajaxResult -> id;
	$SecretCode = $ajaxResult -> value;
	
	$textEdit1 = "
		เรียน ผู้รับบริการ ผู้แจ้งข้อร้องเรียน ที่เคารพเพื่อให้การดำเนินการแจ้งข้อร้องเรีายนของท่านเสร็จสมบูรณ์ ขอควมกรุณาท่านกรอกรหัสที่ท่านได้รับทางหมายเลขโทรศัพท์ที่ท่านระบุไว้ ลงในข้างล่างนี้
		
		";
		
	$textEdit2 = "

		ภายหลังจากที่ท่านยืนยันข้อร้องเรียนผ่านการกรอกรหัสข้างต้นแล้ว ข้อร้องเรียนของท่านจะถูกบันทึกในระบบจัดการข้อร้องเรียน และส่งต่อไปยังเจ้าหน้าที่และผู้บริหารที่เกี่ยวข้อง เพื่อดำเนินการจัดการข้อร้องเรียนของท่านตามแนวทางปฏิบัติของคณะแพทยศาสตร์ต่อไป ท่านจะได้รับอีเมลยืนยันว่าข้อร้องเรียนของท่านได้รับการบันทึกลงในระบบ และเตรียมได้รับการดำเนินการต่อไปแล้ว หลังจากที่ท่านยืนยันข้อร้องเรียนด้วยรหัสผ่าน ภายในระยะเวลา ___1___ วัน หากท่านมีข้อสงสัยด้านการสร้างข้อร้องเรียนและการยืนยันข้อร้องเรียน ท่านสามารถติดต่อได้ที่โทรศัพท์หมายเลข 053-936212 ในวันและเวลาทำการปกติ คือ ระหว่างวันจันทร์ ถึง วันศุกร์ เวลา 8.00 – 16.00 น. คณะแพทยศาสตร์ขอขอบพระคุณท่านเป็นอย่างสูง ที่กรุณาสละเวลาแจ้งข้อร้องเรียนและข้อเสนอแนะ เพื่อการปรับปรุงและพัฒนาการดำเนินการของคณะแพทยศาสตร์ให้ดียิ่ง ๆ ขึ้นไป

		ด้วยความเคารพอย่างสูง

		ทีมงานจัดการข้อร้องเรียน คณะแพทยศาสตร์ มหาวิทยาลัยเชียงใหม่

				";
	
		//** Send Email
		$to = $_POST['email'];
		$subject = "VOC : Security Code";
		$txt = " ".$textEdit1." รหัสความปลอดภัย : ".$SecretCode."".$SecretId."" . $textEdit2;
		$headers = "from : VOC System " . "\r\n";
		SendEmail($to,$subject,$txt,$headers);
		
		//** Send SMS	
		$tel = $_POST['phone'];
		$txt = " VOC รหัสความปลอดภัย : ".$SecretCode."".$SecretId."";
		//$txt = " test api sent sms voc 2";
		SendSMS($tel,$txt);
		
}else if($func == "SendOTP"){
	$ajaxResult = SendOTP($ajaxResult);
}

//*** Return Data */
echo json_encode($ajaxResult);
?>



