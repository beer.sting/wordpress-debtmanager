<?php
session_start();
$document_root = str_replace("\\", "/", explode("wp-content", dirname(__FILE__))[0]);
require_once($document_root . 'wp-content/plugins/debt-collection/includes/config.php');
require_once($document_root . 'wp-content/plugins/debt-collection/model/AjaxResult.php');
require_once($document_root . 'wp-config.php');
require_once($document_root . 'wp-includes/class-phpass.php');

global $wpdb;
date_default_timezone_set('Asia/Bangkok');

function execute()
{
	global $keySystem, $ajaxResult, $wpdb;
	try {
		$ajaxResult = new AjaxResult();
		$func = $_POST['func'];
		$action = $_POST['action'];
		$tablename = $wpdb->prefix . explode("_", $_POST['table'])[1] . "_" . explode("_", $_POST['table'])[2];
		$data = $_POST['data'];
		$dateTime = date("Y-m-d H:i:s");

		// $_POST = array(
		// 	'idcard' => $_POST['idcard'],
		// 	'func' => "Database",
		// 	'action' => "Insert",
		// 	'table' => "Customer",
		// 	'email' => $_POST['email'],
		// 	'phone' => "0846699722",
		// 	'sex' => $_POST['sex'],
		// 	'createdBy' => "system",
		// 	'createDate' => $dateTime
		// );
		// var_dump($_POST);

		//** Customize data
		$data = new ArrayObject;
		foreach ($_POST as $key => $value) {
			if (is_array($key)) {
				foreach ($key as $key2 => $value2) {
					if(!array_search($key2, $keySystem) && is_bool(array_search($key2, $keySystem))){
						if($value2) $data[$key][$key2] = $value2;
					} 
				}
			} else {
				if(!array_search($key, $keySystem) && is_bool(array_search($key, $keySystem))){
					if($value) $data[$key] = $value;
					//if($value) $data[$key ."_check"] = array_search($key, $keySystem);
				} 
			}
		}
		$data = (array) $data;
		//var_dump($data);

		//** Check Function
		switch ($func) {
			case "Database":
				switch ($action) {
					case "Insert":
						$data['createdBy'] ="system";
						$data['createDate'] = $dateTime;
						$wpdb->insert($tablename, $data);
						break;
					case "Update":
						break;
					case "Delete":
						break;
				}
				break;
		}
		$ajaxResult->value = $data;
		$ajaxResult->text = "Save success.";
		$ajaxResult->code = "200";
		$ajaxResult->status = "Success";
		echo json_encode($ajaxResult);
	} catch (Exception $e) {
		$ajaxResult->code = "500";
		$ajaxResult->status = "Error";
		$ajaxResult->text = "Error: " . $e->getMessage();
	}
}

execute();
//add_action("wp_ajax_myxhr", "myajax");