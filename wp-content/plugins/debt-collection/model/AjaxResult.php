<?php
	class AjaxResult
	{
			public $id;
			public $code;
			public $status;
			public $name;
			public $text;
			public $value;
			public $comment;
			public $create_Date;
	}
	
	
	function get_client_ip() {
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CF_CONNECTING_IP']))
			$ipaddress =$_SERVER['HTTP_CF_CONNECTING_IP']; //*** IP Client from Cloudflare
		else if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}

	function CallAPI($method, $url, $data, $authorization = null, $dataType = null)
	{
		$cur = null;
		$result = null;
		if($method != "GET"){
			$curl = curl_init();
		}
		switch ($method)
		{
			case "POST":
				if($dataType == "json"){
					curl_setopt($curl, CURLOPT_POST, 1); 
					curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE));
				}else{
					curl_setopt($curl, CURLOPT_POST, count($data));			
					$data_string = http_build_query($data);
					if ($data)
						curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
				}
				break;
			case "PUT":
				curl_setopt($curl, CURLOPT_PUT, true);
				break;
			default:
				if ($data)
					$url = sprintf("%s?%s", $url, http_build_query($data));
		}
		//*Optional Authentication:
		if($authorization != null){
			 curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); 
		}
		
		if($method == "GET"){
			$result = file_get_contents( $url );
		}else{
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			$result = curl_exec($curl);
			curl_close($curl);
		}
		return $result;
	}
	
?>
