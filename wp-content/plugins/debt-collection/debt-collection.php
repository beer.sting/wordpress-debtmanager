<?php

/**
 * Plugin Name: Debt Collection Addon
 * Description: Addon plugin for Debt Collection Application
 * Plugin URI: https://beerstingshowtime.com
 * Author: BeerSting, IOST COMPANY LIMITED
 * Author URI: https://www.beerstingshowtime.com
 * Version: 1.0
 * License: GPL2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: Debt Collection
 * Domain Path: /languages
 */

/**
 * Copyright (c) 2019 IOST COMPANY LIMITED (email: beer.sting@gmail.com). All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */


class Addon
{

	public function init_plugin()
	{
		add_shortcode('deptAddon', array($this, 'shortcode_Addon'));
		//add_action( 'init', array( $this, 'hmFunction' )); 

		// add_action("wp_ajax_myxhr", "myajax");
		// add_action('wp_ajax_nopriv_get_data', 'my_ajax_handler');
		// add_action('wp_ajax_get_data', 'my_ajax_handler');
	}

	// function my_ajax_handler()
	// {
	// 	wp_send_json_success('It works');
	// }

	public function shortcode_Addon($atts)
	{
		//var_dump($atts);
		$id = $atts['id'];
		$page = $atts['page'];
		$current_user = wp_get_current_user();
		switch (strtolower($id)) {
			case "userlogin":
				return $current_user->user_login;
				break;
			case "userfirstname":
				return $current_user->user_firstname;
				break;
			case "userlastname":
				return $current_user->user_lastname;
				break;
			case "userdisplayname":
				return $current_user->display_name;
				break;
			case "biographicalinfo":
				if ($current_user->exists()) {
					$userdata = get_user_meta($current_user->data->ID);
					return $userdata['description'][0];
				}
				break;
			case "view":
				view($page);
				break;
		}
	}
}

function view($page)
{
	include_once 'view/shared/library.php';
	include_once 'view/' . $page . '/index.php';
}

$addon = new Addon();
$addon->init_plugin();
