<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wp_debt' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wp_debt@ssw0rd' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ' <gQ2=(|fU4,!bm?OkVvyN(*a0C5lm4s.dAv212:fX+sF4RC[g^~S*-3<8T^cMEn' );
define( 'SECURE_AUTH_KEY',  '%SlN}9KH}@SY]EOA.Iy2+fD`n?~S_y3~`7kePoiOu9@`4F&ZH5Ni!`OR`Fl(evzq' );
define( 'LOGGED_IN_KEY',    'v4M%WpE{_LegzCaG[6o[=z` m}9[W%yXP=@>guv:~$1P)+7>-~4s}>YO/p@`c2& ' );
define( 'NONCE_KEY',        'jC2v*efI-(w~Ik0aWan0vrPB%/C-J9M p<ZlKb1)?u7)Z_Bo_}=38l]+E9CN8g*3' );
define( 'AUTH_SALT',        ' |SL#GF)Mu-K!Eg|7`xVGVX/=H`ySMw~4d`zk#aJEO[EJ/N3cwOV3Sft=>0e(w4s' );
define( 'SECURE_AUTH_SALT', '^%<.<3)tL@b0c4.bkzF{}wW6bFgO&fJiKayJ(?9qtZM[j/2bFZ7SSfv9$P->2x/_' );
define( 'LOGGED_IN_SALT',   'hdfV&U3GUe7EIUQOc3k8f1/.Bv3v$ukv]-WF53>0:>_sfAWP*7>PT#7)50<O:d&h' );
define( 'NONCE_SALT',       'aOD_n9kCDs0Gz%{bSEhOwWy[@|<>ka<BC?-xVIiw9,hDegC>s7v)!JiW4X*-AF}o' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
